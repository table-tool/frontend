package com.timewasters.tabletool;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.main.Navigation;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.main.city.PickCityActivity;
import com.timewasters.tabletool.main.market.TrackedProductsActivity;
import com.timewasters.tabletool.main.meeting.ArchivedMeetingsActivity;
import com.timewasters.tabletool.main.meeting.MyMeetingsActivity;
import com.timewasters.tabletool.main.meeting.PlannedMeetingsActivity;
import com.timewasters.tabletool.main.settings.SettingsActivity;
import com.timewasters.tabletool.service.GamesService;
import com.timewasters.tabletool.service.UsersService;
import com.timewasters.tabletool.service.dto.game.GameDTO;
import com.timewasters.tabletool.service.dto.user.UserDTO;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity
{
    private FirebaseAuth mAuthenticator;

    private UsersService mUsersService;

    private TextView nickname;
    private TextView rating;

    private Button myMeetingsButton;
    private Button settingsButton;
    private Button cityButton;
    private Button plannedMeetingsButton;
    private Button archivedMeetingsButton;
    private Button trackedProductsButton;
    private ImageButton moreOptionsButton;

    private Navigation navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        AndroidThreeTen.init(this);

        mAuthenticator = FirebaseAuth.getInstance();

        FirebaseUser user = mAuthenticator.getCurrentUser();

        if (user != null)
        {
            setContentView(R.layout.main_logged_page);

            navigation = new Navigation(this, R.id.person);
            initializeLayoutVariables();

            initializeProfile();

            if (Static.city == null)
            {
                Static.city = mAuthenticator.getCurrentUser().getDisplayName();
            }

            myMeetingsButton.setOnClickListener(view ->
                    ActivityStarter.startActivity(this, MyMeetingsActivity.class));

            settingsButton.setOnClickListener(view ->
                    ActivityStarter.startActivity(this, SettingsActivity.class));

            cityButton.setOnClickListener(view ->
                    {ActivityStarter.startActivity(this, PickCityActivity.class);});

            plannedMeetingsButton.setOnClickListener(view ->
                    ActivityStarter.startActivity(this, PlannedMeetingsActivity.class));

            archivedMeetingsButton.setOnClickListener(view ->
                    ActivityStarter.startActivity(this, ArchivedMeetingsActivity.class));

            trackedProductsButton.setOnClickListener(view ->
                    ActivityStarter.startActivity(this, TrackedProductsActivity.class));

            moreOptionsButton.setOnClickListener(view ->
                    Static.showPopup(view, this));
        }
        else
            ActivityStarter.startActivity(this, WelcomeActivity.class);
    }

    void initializeLayoutVariables()
    {
        nickname = findViewById(R.id.nickname);
        rating = findViewById(R.id.rating);

        myMeetingsButton = findViewById(R.id.my_games_but);
        settingsButton = findViewById(R.id.settings_but);
        cityButton = findViewById(R.id.city_but);
        plannedMeetingsButton = findViewById(R.id.planned_games_but);
        archivedMeetingsButton = findViewById(R.id.game_history_but);
        trackedProductsButton = findViewById(R.id.fav_items_but);
        moreOptionsButton = findViewById(R.id.more_opt_but);
    }

    void initializeProfile()
    {
        MainActivity thisReference = this;

        FirebaseMessaging.getInstance().subscribeToTopic(mAuthenticator.getCurrentUser().getUid()).addOnCompleteListener(task ->
        {
            if (!task.isSuccessful())
            {
               Static.signOut(thisReference);
            }
        });

        if (Static.nick != null && Static.rating != null)
        {
            nickname.setText(Static.nick);
            rating.setText(Static.rating);
        }
        else
        {
            mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task1 ->
            {
                if (task1.isSuccessful())
                {
                    String token = task1.getResult().getToken();

                    mUsersService = Client.getClient(token).create(UsersService.class);

                    Call<UserDTO> userById = mUsersService.userById(mAuthenticator.getCurrentUser().getUid());

                    userById.enqueue(new Callback<UserDTO>()
                    {
                        @Override
                        public void onResponse(@NonNull Call<UserDTO> call, @NonNull Response<UserDTO> response)
                        {
                            if (response.isSuccessful())
                            {
                                UserDTO user = response.body();

                                nickname.setText(user.username);
                                rating.setText(user.rating + "");

                                Static.nick = user.username;
                                Static.rating = user.rating + "";

                                thisReference.receiveGameList();
                            }
                            else
                            {
                                try {
                                    System.out.println(response.errorBody().string());
                                } catch (IOException exception) {
                                    exception.printStackTrace();
                                }

                                Static.signOut(thisReference);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<UserDTO> call, @NonNull Throwable t)
                        {
                            Static.signOut(thisReference);
                        }
                    });
                }
                else
                {
                    System.out.println(task1.getException().toString());

                    Static.signOut(thisReference);
                }
            });
        }
    }

    void receiveGameList()
    {
        if (Static.games != null)
            return;

        MainActivity thisReference = this;

        mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                String token = task.getResult().getToken();

                GamesService mGamesService = Client.getClient(token).create(GamesService.class);

                Call<List<GameDTO>> gamesList = mGamesService.allGames();

                gamesList.enqueue(new Callback<List<GameDTO>>()
                {
                    @Override
                    public void onResponse(@NonNull Call<List<GameDTO>> call, @NonNull Response<List<GameDTO>> response)
                    {
                        if (response.isSuccessful())
                        {
                            Static.games = response.body();
                        }
                        else
                        {
                            Static.signOut(thisReference);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<GameDTO>> call, @NonNull Throwable t)
                    {
                        Static.signOut(thisReference);
                    }
                });
            }
            else
            {
                Static.signOut(thisReference);
            }
        });
    }
}
