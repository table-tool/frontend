package com.timewasters.tabletool.service.dto.meeting;

import com.google.gson.annotations.SerializedName;
import com.timewasters.tabletool.service.dto.user.UserDTO;

import java.util.ArrayList;

public class PageMeetingDTO
{
    @SerializedName("items")
    public ArrayList<MeetingDTO> items;
    @SerializedName("page_number")
    public Integer pageNumber;
    @SerializedName("page_size")
    public Integer pageSize;
    @SerializedName("total_elements")
    public Integer totalElements;
}
