package com.timewasters.tabletool.service.dto.meeting;

import com.google.gson.annotations.SerializedName;
import com.timewasters.tabletool.service.dto.user.UserDTO;

import java.util.List;


public class CreateMeetingDTO
{
    @SerializedName("address")
    public String address;
    @SerializedName("description")
    public String description;
    @SerializedName("period")
    public Integer period;
    @SerializedName("max_players_count")
    public Integer maxPlayerCount;
    @SerializedName("organizer_id")
    public String organizerId;
    @SerializedName("chat_id")
    public String chatId;
    @SerializedName("start_date")
    public String startDate;
    @SerializedName("games")
    public List<String> games;
    @SerializedName("categories")
    public List<String> categories;

    public CreateMeetingDTO(String address, String description, Integer period,
                            Integer maxPlayerCount, String organizerId, String chatId,
                            String startDate, List<String> games, List<String> categories)
    {
        this.address = address;
        this.description = description;
        this.period = period;
        this.maxPlayerCount = maxPlayerCount;
        this.organizerId = organizerId;
        this.chatId = chatId;
        this.startDate = startDate;
        this.games = games;
        this.categories = categories;
    }

    public String toString()
    {
        return "address: " + address + "\ndescription: " + description + "\nperiod: "
                + period + "\nmax_players_count: " + maxPlayerCount + "\norganizer_id: "
                + organizerId + "\nstart_date: " + startDate + "\ngames: " + games.toString() + "\ncategories: " + categories.toString();
    }
}
