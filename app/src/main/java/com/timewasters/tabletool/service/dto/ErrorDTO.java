package com.timewasters.tabletool.service.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ErrorDTO
{
    @SerializedName("http_status")
    public String status;
    @SerializedName("messages")
    public List<String> messages;
    @SerializedName("time")
    public String time;

    public ErrorDTO(String status, List<String> messages, String time)
    {
        this.status = status;
        this.messages = messages;
        this.time = time;
    }
}
