package com.timewasters.tabletool.service;

import com.timewasters.tabletool.service.dto.product.ProductDTO;
import com.timewasters.tabletool.service.dto.product.TrackedProductDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ProductService
{
    @GET("/products")
    Call<List<ProductDTO>> searchProducts(@Query("search") String query);

    @GET("/products/tracked")
    Call<List<ProductDTO>> trackedProducts();

    @POST("/products/tracked")
    Call<TrackedProductDTO> addToTrackable(@Body TrackedProductDTO product);
}
