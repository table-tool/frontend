package com.timewasters.tabletool.service.dto.game;

import com.google.gson.annotations.SerializedName;

public class GameDTO
{
    @SerializedName("name")
    public String name;
    @SerializedName("image")
    public String image;

    public GameDTO(String name, String image)
    {
        this.name = name;
        this.image = image;
    }
}
