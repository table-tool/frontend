package com.timewasters.tabletool.service.dto.product;

import android.os.Bundle;

import com.google.gson.annotations.SerializedName;

public class TrackedProductDTO
{
    @SerializedName("name")
    public String name;
    @SerializedName("price")
    public Integer price;
    @SerializedName("link")
    public String link;
    @SerializedName("photo_link")
    public String imageLink;
    @SerializedName("user_id")
    public String userId;

    public TrackedProductDTO(String name, Integer price, String link, String imageLink, String userId)
    {
        this.name = name;
        this.price = price;
        this.link = link;
        this.imageLink = imageLink;
        this.userId = userId;
    }

    public TrackedProductDTO(ProductDTO product, String userId)
    {
        this.name = product.name;
        this.price = product.price;
        this.link = product.link;
        this.imageLink = product.imageLink;
        this.userId = userId;
    }

    public Bundle pack()
    {
        Bundle info = new Bundle();

        info.putString("name", name);
        info.putString("price", price + "");
        info.putString("link", link);
        info.putString("imageLink", imageLink);
        info.putString("userId", userId);

        return info;
    }
}
