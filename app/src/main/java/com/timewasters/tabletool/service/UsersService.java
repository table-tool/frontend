package com.timewasters.tabletool.service;

import com.timewasters.tabletool.service.dto.user.UpdateUserDTO;
import com.timewasters.tabletool.service.dto.user.IsUniqueNicknameDTO;
import com.timewasters.tabletool.service.dto.user.UserDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface UsersService
{
    @POST("/users")
    Call<UserDTO> createUser(@Body UpdateUserDTO dto);

    @PUT("/users")
    Call<UserDTO> updateUser(@Body UpdateUserDTO nickname);

    @GET("/users/username")
    Call<IsUniqueNicknameDTO> isUniqueNickname(@Query("username") String nickname);

    @GET("/users/{user_id}")
    Call<UserDTO> userById(@Path("user_id") String user_id);

    @POST("/ratings/meeting/{meeting_id}/player/{player_id}/rate/{rate}")
    Call<Void> rateUser(@Path("meeting_id") String meetingId, @Path("player_id") String playerId,
                        @Path("rate") String rate);


    @DELETE("/ratings/meeting/{meeting_id}/player/{player_id}}")
    Call<Void> unrateUser(@Path("meeting_id") String meetingId, @Path("player_id") String playerId);

    @POST("/users/{user_id}/meeting/{meeting_id}")
    Call<Void> addToMeeting(@Path("user_id") String userId, @Path("meeting_id") String meetingId);

    @DELETE("/users/{user_id}/meeting/{meeting_id}")
    Call<Void> deleteFromMeeting(@Path("meeting_id") String meetingId, @Path("user_id") String userId);
}

