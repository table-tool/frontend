package com.timewasters.tabletool.service.dto.user;

import com.google.gson.annotations.SerializedName;

public class UpdateUserDTO
{
    @SerializedName("username")
    public String username;
    @SerializedName("user_id")
    public String user_id;

    public UpdateUserDTO(String username, String user_id)
    {
        this.username = username;
        this.user_id = user_id;
    }
}
