package com.timewasters.tabletool.service;

import com.timewasters.tabletool.service.dto.game.GameDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GamesService
{
    @GET("/games")
    Call<List<GameDTO>> allGames();

    @GET("/categories")
    Call<List<String>> allCategories();
}
