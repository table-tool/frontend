package com.timewasters.tabletool.service.dto.meeting;

import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.annotations.SerializedName;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.service.dto.user.UserDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MeetingDTO
{
    @SerializedName("address")
    public String address;
    @SerializedName("description")
    public String description;
    @SerializedName("period")
    public Integer period;
    @SerializedName("max_players_count")
    public Integer maxPlayerCount;
    @SerializedName("organizer_id")
    public String organizerId;
    @SerializedName("chat_id")
    public String chatId;
    @SerializedName("meeting_id")
    public String meetingId;
    @SerializedName("organizer_rating")
    public String organizerRating;
    @SerializedName("start_date")
    public String startDate;
    @SerializedName("games")
    public List<String> games;
    @SerializedName("categories")
    public List<String> categories;
    @SerializedName("members")
    public List<UserDTO> members;
    @SerializedName("users_rating")
    public List<RatingDTO> usersRating;

    public MeetingDTO(String address, String description, Integer period, Integer maxPlayerCount,
                      String organizerId, String chatId, String meetingId, String organizerRating, String startDate,
                      List<String> games, List<String> categories, List<UserDTO> members,
                      List<RatingDTO> usersRating)
    {
        this.startDate = startDate;
        this.address = address;
        this.description = description;
        this.period = period;
        this.maxPlayerCount = maxPlayerCount;
        this.organizerId = organizerId;
        this.chatId = chatId;
        this.meetingId = meetingId;
        this.organizerRating = organizerRating;
        this.games = games;
        this.categories = categories;
        this.members = members;
        this.usersRating = usersRating;
    }

    public Bundle pack()
    {
        MeetingDTO meeting = this;

        ArrayList<String> members = new ArrayList<>();
        ArrayList<String> membersIds = new ArrayList<>();
        ArrayList<String> membersRatings = new ArrayList<>();

        for (UserDTO user : meeting.members)
        {
            members.add(user.username);
            membersIds.add(user.uid);
            membersRatings.add(user.rating + "");
        }

        List<RatingDTO> temp = meeting.usersRating;

        List<RatingDTO> filtered = temp.stream()
                .filter(element -> element.evalId.equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))
                .collect(Collectors.toList());

        ArrayList<String> ratedIds = new ArrayList<>();
        ArrayList<String> myRatings = new ArrayList<>();

        for (RatingDTO rating : filtered)
        {
            ratedIds.add(rating.userId);
            myRatings.add(rating.ratingMark);
        }

        Bundle extras = new Bundle();
        extras.putString("name", meeting.games.get(0));
        extras.putString("playerCount", meeting.members.size() + "");
        extras.putString("maxPlayerCount", meeting.maxPlayerCount + "");
        extras.putString("date", meeting.startDate);
        extras.putString("address", meeting.address);
        extras.putString("description", meeting.description);

        extras.putStringArrayList("members", members);
        extras.putStringArrayList("membersIds", membersIds);
        extras.putStringArrayList("membersRatings", membersRatings);
        extras.putStringArrayList("ratedIds", ratedIds);
        extras.putStringArrayList("myRatings", myRatings);

        extras.putString("meetingId", meeting.meetingId);
        extras.putString("organizerId", meeting.organizerId);

        return extras;
    }
}
