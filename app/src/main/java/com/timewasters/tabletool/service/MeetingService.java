package com.timewasters.tabletool.service;

import com.timewasters.tabletool.service.dto.meeting.CreateMeetingDTO;
import com.timewasters.tabletool.service.dto.meeting.MeetingDTO;
import com.timewasters.tabletool.service.dto.meeting.PageMeetingDTO;
import com.timewasters.tabletool.service.dto.meeting.UpdateMeetingDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MeetingService
{
    @POST("/meetings")
    Call<MeetingDTO> createMeeting(@Body CreateMeetingDTO meeting);

    @GET("/meetings")
    Call<PageMeetingDTO> usersMeetings(@Query("user_id") String userId,
                                       @Query("meeting_status") String status,
                                       @Query("page_size") Integer pageSize,
                                       @Query("organizer_id") String organizerId);

    @GET("/meetings")
    Call<PageMeetingDTO> usersMeetings(@Query("user_id") String userId,
                                       @Query("meeting_status") String status,
                                       @Query("page_size") Integer pageSize,
                                       @Query("organizer_id") String organizerId,
                                       @Query("categories") List<String> categories,
                                       @Query("address") String address,
                                       @Query("rating_state_start") String ratingStart,
                                       @Query("rating_state_end") String ratingEnd,
                                       @Query("search_string") String query);

    @PUT("/meetings")
    Call<MeetingDTO> updateMeeting(@Body UpdateMeetingDTO meeting);

    @GET("/meetings/{meeting_id}")
    Call<MeetingDTO> meetingById(@Path("meeting_id") String meetingId);

    @DELETE("/meetings/{meeting_id}")
    Call<Void> deleteMeeting(@Path("meeting_id") String meetingId);
}
