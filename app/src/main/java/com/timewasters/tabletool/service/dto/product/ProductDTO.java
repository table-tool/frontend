package com.timewasters.tabletool.service.dto.product;

import android.os.Bundle;

import com.google.gson.annotations.SerializedName;

public class ProductDTO
{
    @SerializedName("name")
    public String name;
    @SerializedName("price")
    public Integer price;
    @SerializedName("link")
    public String link;
    @SerializedName("photo_link")
    public String imageLink;

    public ProductDTO(String name, Integer price, String link, String imageLink)
    {
        this.name = name;
        this.price = price;
        this.link = link;
        this.imageLink = imageLink;
    }

    public ProductDTO(Bundle info)
    {
        name = info.getString("name");
        price = Integer.parseInt(info.getString("price"));
        link = info.getString("link");
        imageLink = info.getString("imageLink");
    }

    public TrackedProductDTO toTracked(String id)
    {
        return new TrackedProductDTO(this, id);
    }

    public Bundle pack()
    {
        Bundle info = new Bundle();

        info.putString("name", name);
        info.putString("price", price + "");
        info.putString("link", link);
        info.putString("imageLink", imageLink);

        return info;
    }
}
