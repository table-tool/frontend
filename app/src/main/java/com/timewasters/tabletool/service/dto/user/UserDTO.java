package com.timewasters.tabletool.service.dto.user;

import com.google.gson.annotations.SerializedName;

public class UserDTO
{
    @SerializedName("username")
    public String username;
    @SerializedName("rating")
    public Integer rating;
    @SerializedName("user_id")
    public String uid;

    public UserDTO(String username, String uid)
    {
        this.username = username;
        this.uid = uid;
    }
}
