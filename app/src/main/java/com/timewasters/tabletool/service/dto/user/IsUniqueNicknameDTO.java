package com.timewasters.tabletool.service.dto.user;

import com.google.gson.annotations.SerializedName;

public class IsUniqueNicknameDTO
{
    @SerializedName("valid")
    public boolean valid;
}
