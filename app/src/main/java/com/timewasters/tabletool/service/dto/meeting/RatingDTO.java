package com.timewasters.tabletool.service.dto.meeting;

import com.google.gson.annotations.SerializedName;

public class RatingDTO
{
    @SerializedName("user_id")
    public String userId;
    @SerializedName("evaluator_id")
    public String evalId;
    @SerializedName("rating_mark")
    public String ratingMark;
}
