package com.timewasters.tabletool.authentication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;

import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;

public class PasswordResetConfirmationActivity extends AppCompatActivity
{
    private Button mToMainScreenButton;

    private ImageButton mPreviousScreenButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restore_pass_confirmation);

        initializeLayoutVariables();

        PasswordResetConfirmationActivity thisReference = this;

        mToMainScreenButton.setOnClickListener(view -> ActivityStarter.startWelcomeActivity(thisReference));

        mPreviousScreenButton.setOnClickListener(view -> ActivityStarter.startPasswordResetActivity(thisReference));
    }

    void initializeLayoutVariables()
    {
        mToMainScreenButton = findViewById(R.id.login_button);

        mPreviousScreenButton = findViewById(R.id.back_button);
    }
}