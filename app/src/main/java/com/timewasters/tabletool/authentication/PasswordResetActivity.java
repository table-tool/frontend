package com.timewasters.tabletool.authentication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.error.AuthenticationError;

public class PasswordResetActivity extends AppCompatActivity
{
    private EditText mEmailText;

    private TextView mErrorText;

    private Button mSubmitResetButton;

    private ImageButton mPreviousScreenButton;

    private FirebaseAuth mAuthenticator;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restore_password_page);

        initializeLayoutVariables();

        mAuthenticator = FirebaseAuth.getInstance();

        PasswordResetActivity thisReference = this;

        mSubmitResetButton.setOnClickListener(view ->
        {
            String emailText = mEmailText.getText().toString();

            if (emailText.isEmpty())
            {
                mErrorText.setText(AuthenticationError.EMPTY_EMAIL.toString());

                return;
            }

            mAuthenticator.setLanguageCode("ru");
            mAuthenticator.sendPasswordResetEmail(emailText).addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                    ActivityStarter.startPasswordResetConfirmationActivity(thisReference);
                else
                    task.getException().toString();
                    mErrorText.setText(AuthenticationError.EMAIL_DO_NOT_EXIST.toString());
            });
        });

        mPreviousScreenButton.setOnClickListener(view -> ActivityStarter.startLoginActivity(thisReference));
    }

    void initializeLayoutVariables()
    {
        mEmailText = findViewById(R.id.email_restore_field);

        mErrorText = findViewById(R.id.error_text_block);

        mSubmitResetButton = findViewById(R.id.login_button);

        mPreviousScreenButton = findViewById(R.id.back_button);
    }
}