package com.timewasters.tabletool.authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.client.Client;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.timewasters.tabletool.error.*;
import com.timewasters.tabletool.service.dto.ErrorDTO;
import com.timewasters.tabletool.service.dto.user.UpdateUserDTO;
import com.timewasters.tabletool.service.dto.user.IsUniqueNicknameDTO;
import com.timewasters.tabletool.service.UsersService;
import com.timewasters.tabletool.service.dto.user.UserDTO;


public class RegistrationActivity extends AppCompatActivity
{
    private EditText mLoginText;
    private EditText mNicknameText;
    private EditText mPasswordText;
    private EditText mPasswordConfirmationText;

    private TextView mErrorText;

    private Button mRegisterButton;

    private ImageButton mPreviousScreenButton;

    private FirebaseAuth mAuthenticator;
    private FirebaseAnalytics mAnalytics;
    private UsersService mUsersService;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_page);

        mAuthenticator = FirebaseAuth.getInstance();
        mAnalytics = FirebaseAnalytics.getInstance(this);

        mUsersService = Client.getClient().create(UsersService.class);

        initializeLayoutVariables();

        RegistrationActivity thisReference = this;

        mPreviousScreenButton.setOnClickListener(view -> ActivityStarter.startWelcomeActivity(thisReference));

        mRegisterButton.setOnClickListener(view ->
        {
            String email = mLoginText.getText().toString();
            String nickname = mNicknameText.getText().toString();
            String password = mPasswordText.getText().toString();
            String password_confirmation = mPasswordConfirmationText.getText().toString();

            if (email.isEmpty())
            {
                mErrorText.setText(AuthenticationError.EMPTY_EMAIL.toString());

                return;
            }

            if (nickname.isEmpty())
            {
                mErrorText.setText(AuthenticationError.EMPTY_NICKNAME.toString());

                return;
            }

            if (password.isEmpty())
            {
                mErrorText.setText(AuthenticationError.EMPTY_PASSWORD.toString());

                return;
            }

            if (password_confirmation.isEmpty())
            {
                mErrorText.setText(AuthenticationError.EMPTY_PASSWORD_CONFIRMATION.toString());

                return;
            }

            if (password.length() < 6)
            {
                mErrorText.setText(AuthenticationError.SHORT_PASSWORD.toString());
                mPasswordText.getText().clear();
                mPasswordConfirmationText.getText().clear();

                return;
            }

            if (!password.equals(password_confirmation))
            {
                mErrorText.setText(AuthenticationError.PASSWORDS_DO_NOT_MATCH.toString());
                mPasswordText.getText().clear();
                mPasswordConfirmationText.getText().clear();

                return;
            }

            if (!isUnique(nickname))
            {
                mErrorText.setText(AuthenticationError.NOT_UNIQUE_NICKNAME.toString());

                return;
            }

            mAuthenticator.createUserWithEmailAndPassword(email, password).addOnCompleteListener(thisReference, task ->
            {
                if (task.isSuccessful())
                {
                    FirebaseUser user = mAuthenticator.getCurrentUser();

                    Call<UserDTO> users = mUsersService.createUser(new UpdateUserDTO(nickname, user.getUid()));

                    users.enqueue(new Callback<UserDTO>()
                    {
                        @Override
                        public void onResponse(@NonNull Call<UserDTO> call, @NonNull Response<UserDTO> response)
                        {
                            if (response.isSuccessful())
                            {
                                Bundle event = new Bundle();
                                event.putString("registered", "true");
                                thisReference.mAnalytics.logEvent("register_event", event);

                                ActivityStarter.startMainActivity(thisReference);
                            }
                            else
                            {
                                user.delete();

                                Gson gson = new Gson();

                                List<String> errors = gson.fromJson(response.errorBody().charStream(), ErrorDTO.class).messages;

                                if (!errors.isEmpty())
                                {
                                    StringBuilder fullError = new StringBuilder();

                                    for (String error : errors)
                                        fullError.append(error).append("\n");


                                    mErrorText.setText(fullError.toString());
                                }
                                else
                                    mErrorText.setText(ServerError.WRONG_INPUT.toString());
                            }

                        }

                        @Override
                        public void onFailure(@NonNull Call<UserDTO> call, @NonNull Throwable t)
                        {
                            mErrorText.setText(ServerError.SERVER_UNREACHABLE.toString());
                        }
                    });
                }
                else
                    mErrorText.setText(AuthenticationError.REGISTRATION_FAILED.toString());
            });
        });
    }

    private void initializeLayoutVariables()
    {
        mLoginText = findViewById(R.id.email_field);
        mNicknameText = findViewById(R.id.nickname_field);
        mPasswordText = findViewById(R.id.pass_field);
        mPasswordConfirmationText = findViewById(R.id.pass_conf_field);
        mErrorText = findViewById(R.id.error_text_block);

        mRegisterButton = findViewById(R.id.registration_button);
        mPreviousScreenButton = findViewById(R.id.back_button);
    }

    private boolean isUnique(String nickname)
    {
        final boolean[] result = {false};
        final boolean[] error = {false};

        Call<IsUniqueNicknameDTO> nicknameQuery = mUsersService.isUniqueNickname(nickname);

        Thread thread = new Thread (()->
        {
            try
            {
                Response<IsUniqueNicknameDTO> response = nicknameQuery.execute();

                if (response.isSuccessful())
                    result[0] = response.body().valid;
            }
            catch (Exception exception)
            {
                error[0] = true;
            }
        });

        thread.start();

        try
        {
            thread.join();
        }
        catch (Exception exception)
        {
            mErrorText.setText(OSError.THREAD_ERROR.toString());
        }

        if (error[0])
            mErrorText.setText(ServerError.SERVER_UNREACHABLE.toString());

        return result[0];
    }
}
