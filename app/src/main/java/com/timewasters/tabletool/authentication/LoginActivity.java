package com.timewasters.tabletool.authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.MainActivity;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.error.AuthenticationError;
import com.timewasters.tabletool.error.ServerError;
import com.timewasters.tabletool.service.UsersService;
import com.timewasters.tabletool.service.dto.user.UserDTO;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity
{
    private ImageButton mPreviousScreenButton;

    private Button mLoginButton;
    private EditText mLoginText;
    private EditText mPasswordText;

    private TextView mErrorText;

    private Button mPasswordResetButton;

    private FirebaseAuth mAuthenticator;
    private FirebaseAnalytics mAnalytics;
    private UsersService mUsersService;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);

        mAuthenticator = FirebaseAuth.getInstance();
        mAnalytics = FirebaseAnalytics.getInstance(this);

        initializeLayoutVariables();

        LoginActivity thisReference = this;

        mPasswordResetButton.setOnClickListener(view -> ActivityStarter.startPasswordResetActivity(thisReference));

        mLoginButton.setOnClickListener(view ->
        {
            String login = mLoginText.getText().toString();
            String password = mPasswordText.getText().toString();

            if (login.isEmpty())
            {
                mErrorText.setText(AuthenticationError.EMPTY_EMAIL.toString());

                return;
            }

            if (password.isEmpty())
            {
                mErrorText.setText(AuthenticationError.EMPTY_PASSWORD.toString());

                return;
            }

            signIn(login, password);
        });

        mPreviousScreenButton.setOnClickListener(view -> ActivityStarter.startWelcomeActivity(thisReference));
    }

    private void initializeLayoutVariables()
    {
        mPreviousScreenButton = findViewById(R.id.back_button);

        mLoginButton = findViewById(R.id.login_button);
        mLoginText = findViewById(R.id.email_login_field);
        mPasswordText = findViewById(R.id.pass_field);

        mErrorText = findViewById(R.id.error_text_block);

        mPasswordResetButton = findViewById(R.id.pass_reset_button);
    }

    private void signIn(String login, String password)
    {
        LoginActivity thisReference = this;

        mAuthenticator.signInWithEmailAndPassword(login, password)
                .addOnCompleteListener(this, task ->
                {
                    if (task.isSuccessful())
                    {
                        mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task1 ->
                        {
                           if (task1.isSuccessful())
                           {
                               String token = task1.getResult().getToken();

                               mUsersService = Client.getClient(token).create(UsersService.class);

                               Call<UserDTO> userById = mUsersService.userById(mAuthenticator.getCurrentUser().getUid());

                               userById.enqueue(new Callback<UserDTO>()
                               {
                                   @Override
                                   public void onResponse(@NonNull Call<UserDTO> call, @NonNull Response<UserDTO> response)
                                   {
                                       if (response.isSuccessful())
                                       {
                                           Bundle event = new Bundle();
                                           event.putString("logged", "true");
                                           thisReference.mAnalytics.logEvent("login_event", event);

                                           UserDTO user = response.body();

                                           Bundle extras = new Bundle();
                                           extras.putString("nick", user.username);
                                           extras.putString("rating", user.rating + "");

                                           ActivityStarter.startActivity(thisReference, MainActivity.class, extras);
                                       }
                                       else
                                       {
                                           mErrorText.setText(ServerError.SERVER_INTERNAL.toString());
                                       }
                                   }

                                   @Override
                                   public void onFailure(@NonNull Call<UserDTO> call, @NonNull Throwable t)
                                   {
                                       mErrorText.setText(ServerError.SERVER_UNREACHABLE.toString());
                                   }
                               });
                           }
                           else
                           {
                               mErrorText.setText(ServerError.SERVER_UNREACHABLE.toString());
                           }
                        });
                    }
                    else
                        mErrorText.setText(AuthenticationError.WRONG_EMAIL_OR_PASSWORD.toString());
                });
    }
}
