package com.timewasters.tabletool.main.meeting;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.error.MeetingError;
import com.timewasters.tabletool.error.ServerError;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.service.GamesService;
import com.timewasters.tabletool.service.MeetingService;
import com.timewasters.tabletool.service.dto.game.GameDTO;
import com.timewasters.tabletool.service.dto.meeting.MeetingDTO;
import com.timewasters.tabletool.service.dto.meeting.UpdateMeetingDTO;

import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.LocalTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditMeetingActivity extends AppCompatActivity
{
    private FirebaseAuth mAuthenticator;
    private MeetingService mMeetingService;
    private GamesService mGamesService;

    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;

    private String date;
    private String time;

    private LocalDate localDate;
    private LocalTime localTime;

    private EditText meetingAddressText;

    private SeekBar meetingPlayerCapacityBar;

    private Button pickDateButton;
    private Button pickHourButton;

    private EditText meetingDescriptionText;

    private Button editMeetingButton;
    private Button deleteMeetingButton;
    private ImageButton backButton;

    private Integer meetingPlayerCapacity;
    private Integer minPlayerCapacity;

    private AutoCompleteTextView pickedGame;
    private TextView pickedPlayerCount;

    private TextView errorText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_game_edit_page);

        AndroidThreeTen.init(this);

        mAuthenticator = FirebaseAuth.getInstance();

        initializeLayoutVariables();

        setDefaultValues();

        initializeGameList();

        pickDateButton.setOnClickListener(view ->
                datePickerDialog.show());

        pickHourButton.setOnClickListener(view ->
                timePickerDialog.show());

        backButton.setOnClickListener(view ->
        {
            Bundle extras = getIntent().getExtras();

            ActivityStarter.startActivity(this, MeetingActivity.class, extras);
        });

        EditMeetingActivity thisReference = this;

        editMeetingButton.setOnClickListener(view ->
        {
            if (localDate.compareTo(LocalDate.now()) < 0)
            {
                errorText.setText(MeetingError.TIME.toString());

                return;
            }

            if (localDate.compareTo(LocalDate.now()) == 0 && localTime.compareTo(LocalTime.now()) < 0)
            {
                errorText.setText(MeetingError.TIME.toString());

                return;
            }

            if (Static.city == null || Static.city.isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_CITY.toString());

                return;
            }

            if (pickedGame.getText().toString().isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_GAME.toString());

                return;
            }

            if (meetingPlayerCapacity == 0 || meetingPlayerCapacity == 1)
            {
                errorText.setText(MeetingError.ONE_MAN_ARMY.toString());

                return;
            }

            if (date.isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_DATE.toString());

                return;
            }

            if (time.isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_TIME.toString());

                return;
            }

            if (meetingAddressText.getText().toString().isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_ADDRESS.toString());

                return;
            }

            if (meetingDescriptionText.getText().toString().isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_DESCRIPTION.toString());

                return;
            }

            String fullDate = date + "T" + time;

            List<String> game = new ArrayList<>();
            game.add(pickedGame.getText().toString());

            mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                {
                    String token = task.getResult().getToken();

                    mMeetingService = Client.getClient(token).create(MeetingService.class);

                    Bundle extras = thisReference.getIntent().getExtras();

                    UpdateMeetingDTO data = new UpdateMeetingDTO
                            (
                                    extras.getString("meetingId"),
                                    Static.city + ", " + meetingAddressText.getText().toString(),
                                    meetingDescriptionText.getText().toString(),
                                    451,
                                    meetingPlayerCapacity,
                                    mAuthenticator.getCurrentUser().getUid(),
                                    "ripnites",
                                    fullDate,
                                    game,
                                    null
                            );

                    Call<MeetingDTO> updateMeeting = mMeetingService.updateMeeting(data);

                    updateMeeting.enqueue(new Callback<MeetingDTO>()
                    {
                        @Override
                        public void onResponse(@NonNull Call<MeetingDTO> call, @NonNull Response<MeetingDTO> response)
                        {
                            if (response.isSuccessful())
                            {
                                Bundle extras = response.body().pack();

                                ActivityStarter.startActivity(thisReference, MeetingActivity.class, extras);
                            }
                            else
                            {
                                errorText.setText(ServerError.SERVER_INTERNAL.toString());
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<MeetingDTO> call, @NonNull Throwable t)
                        {
                            errorText.setText(ServerError.SERVER_UNREACHABLE.toString());
                        }
                    });
                }
                else
                {
                    errorText.setText(ServerError.SERVER_UNREACHABLE.toString());
                }
            });
        });

        deleteMeetingButton.setOnClickListener(view ->
                mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
                {
                    if (task.isSuccessful())
                    {
                        String token = task.getResult().getToken();

                        mMeetingService = Client.getClient(token).create(MeetingService.class);

                        Bundle extras = thisReference.getIntent().getExtras();

                        Call<Void> deleteMeeting = mMeetingService.deleteMeeting(extras.getString("meetingId"));
                        System.out.println(extras.getString("meetingId"));

                        deleteMeeting.enqueue(new Callback<Void>()
                        {
                            @Override
                            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response)
                            {
                                if (response.isSuccessful())
                                {
                                    ActivityStarter.startActivity(thisReference, MyMeetingsActivity.class);
                                }
                                else
                                {
                                    JSONObject er = null;
                                    try {
                                        er = new JSONObject(response.errorBody().string());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (IOException exception) {
                                        exception.printStackTrace();
                                    }

                                    System.out.println(er.toString());

                                    errorText.setText(ServerError.SERVER_INTERNAL.toString());
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t)
                            {
                                errorText.setText(ServerError.SERVER_UNREACHABLE.toString());
                            }
                        });
                    }
                    else
                    {
                        errorText.setText(ServerError.SERVER_UNREACHABLE.toString());
                    }
                }));
    }

    void initializeLayoutVariables()
    {
        meetingAddressText = findViewById(R.id.game_name);

        meetingPlayerCapacityBar = findViewById(R.id.seekBar);

        pickDateButton = findViewById(R.id.pick_date_button);
        pickHourButton = findViewById(R.id.pick_time_button);

        meetingDescriptionText = findViewById(R.id.game_desc_textfield);

        editMeetingButton = findViewById(R.id.confirm_change_button);
        deleteMeetingButton = findViewById(R.id.delete_button);
        backButton = findViewById(R.id.back_button);
        pickedGame = findViewById(R.id.typesFilter);
        pickedPlayerCount = findViewById(R.id.chosen_amount);
        errorText = findViewById(R.id.error_text_block);
    }

    void setDefaultValues()
    {
        Bundle info = getIntent().getExtras();

        String addr = info.getString("address");
        int commaIndex = addr.indexOf(",");
        String lAddr = addr.substring(commaIndex + 2);

        meetingAddressText.setText(lAddr);
        pickedGame.setText(info.getString("name"));
        meetingDescriptionText.setText(info.getString("description"));

        meetingPlayerCapacity = Integer.parseInt(info.getString("maxPlayerCount"));
        minPlayerCapacity = Integer.parseInt(info.getString("playerCount"));

        meetingPlayerCapacityBar.setProgress(meetingPlayerCapacity);
        meetingPlayerCapacityBar.setMax(20);
        pickedPlayerCount.setText(meetingPlayerCapacity + "");

        meetingPlayerCapacityBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b)
            {
                if (seekBar.getProgress() < minPlayerCapacity)
                {
                    seekBar.setProgress(minPlayerCapacity);
                    meetingPlayerCapacity = minPlayerCapacity;

                    return;
                }

                meetingPlayerCapacity = seekBar.getProgress();

                pickedPlayerCount.setText(meetingPlayerCapacity + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        String [] fullDate = info.getString("date").split("T");

        date = fullDate[0];
        time = fullDate[1];

        pickDateButton.setText(date);
        pickHourButton.setText(time);

        localDate = LocalDate.parse(date);
        localTime = LocalTime.parse(time);

        date = localDate.toString();
        pickDateButton.setText(date);

        time = localTime.toString();
        pickHourButton.setText(time);

        DatePickerDialog.OnDateSetListener listener = (datePicker, year, month, day) ->
        {
            localDate = LocalDate.of(year, month + 1, day);
            date = localDate.toString();

            pickDateButton.setText(date);
        };

        TimePickerDialog.OnTimeSetListener timeSetListener = (timePicker, hour, minute) ->
        {
            localTime = LocalTime.of(hour, minute);
            time = localTime.toString();

            pickHourButton.setText(time);
        };

        LocalDateTime d = LocalDateTime.now();

        datePickerDialog = new DatePickerDialog(this, listener, d.getYear(), d.getMonthValue() - 1, d.getDayOfMonth());
        timePickerDialog = new TimePickerDialog(this, timeSetListener, 12, 0, true);
    }

    private void initializeGameList()
    {
        EditMeetingActivity thisReference = this;

        mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                String token = task.getResult().getToken();

                mGamesService = Client.getClient(token).create(GamesService.class);

                Call<List<GameDTO>> gamesList = mGamesService.allGames();

                gamesList.enqueue(new Callback<List<GameDTO>>()
                {
                    @Override
                    public void onResponse(@NonNull Call<List<GameDTO>> call, @NonNull Response<List<GameDTO>> response)
                    {
                        if (response.isSuccessful())
                        {
                            List<GameDTO> games = response.body();
                            ArrayList<String> gameNames = new ArrayList<>();

                            for (GameDTO game : games)
                            {
                                gameNames.add(game.name);
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<>(thisReference, R.layout.dropdown_menu_popup_item, gameNames);

                            pickedGame.setAdapter(adapter);
                        }
                        else
                        {
                            errorText.setText(ServerError.SERVER_INTERNAL.toString());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<GameDTO>> call, @NonNull Throwable t)
                    {
                        errorText.setText(ServerError.SERVER_UNREACHABLE.toString());
                    }
                });
            }
            else
            {
                errorText.setText(ServerError.SERVER_UNREACHABLE.toString());
            }
        });
    }
}