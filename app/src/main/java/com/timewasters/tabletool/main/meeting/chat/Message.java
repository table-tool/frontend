package com.timewasters.tabletool.main.meeting.chat;

public class Message
{
    public String message;
    public String sender;
    public String date;
    public String time;

    public Message() {}

    public Message(String message, String sender, String date, String time)
    {
        this.message = message;
        this.sender = sender;
        this.date = date;
        this.time = time;
    }
}
