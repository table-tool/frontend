package com.timewasters.tabletool.main.meeting;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.WelcomeActivity;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.service.UsersService;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArchivedMeetingActivity extends AppCompatActivity
{
    private TextView name;
    private TextView playersCount;
    private TextView date;
    private TextView time;
    private TextView address;
    private TextView city;

    private RecyclerView playerList;
    PlayersAdapter adapter;

    private ImageButton backButton;
    private ImageButton moreOptionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_history_details_page);

        initializeLayoutVariables();
        initializePage();

        backButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, ArchivedMeetingsActivity.class));

        Bundle info = getIntent().getExtras();

        List<String> memberNames = info.getStringArrayList("members");
        List<String> memberIds = info.getStringArrayList("membersIds");

        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        if (memberIds.contains(uid))
        {
            int index = memberIds.indexOf(uid);

            memberIds.remove(index);
            memberNames.remove(index);
        }

        List<String> ratedIds = info.getStringArrayList("ratedIds");
        List<String> myRatings = info.getStringArrayList("myRatings");

        playerList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ArchivedMeetingActivity
                .PlayersAdapter(this, memberNames, memberIds,
                info.getString("meetingId"), this, ratedIds, myRatings);

        playerList.setAdapter(adapter);

        moreOptionsButton.setOnClickListener(view ->
                Static.showPopup(view, this));
    }

    void initializeLayoutVariables()
    {
        name = findViewById(R.id.game_name);
        playersCount = findViewById(R.id.players_amount);
        date = findViewById(R.id.date_label);
        time = findViewById(R.id.time_label);
        address = findViewById(R.id.address_label);
        city = findViewById(R.id.city_label);

        playerList = findViewById(R.id.players_list);

        backButton = findViewById(R.id.back_button);
        moreOptionsButton = findViewById(R.id.more_opt_but);
    }

    void initializePage()
    {
        Bundle info = getIntent().getExtras();

        name.setText(info.getString("name"));
        playersCount.setText(info.getString("playerCount") + "/" + info.getString("maxPlayerCount") + " участников");

        String [] fullDate = info.getString("date").split("T");
        date.setText(fullDate[0]);
        time.setText(fullDate[1]);

        String addr = info.getString("address");
        int commaIndex = addr.indexOf(",");
        String lCity = addr.substring(0, commaIndex);
        String lAddr = addr.substring(commaIndex + 2);

        address.setText(lAddr);
        city.setText(lCity);
    }

    static class PlayersAdapter extends RecyclerView.Adapter<ArchivedMeetingActivity.PlayersAdapter.ViewHolder>
    {
        private final List<String> mMembers;
        private final List<String> mIds;
        private final LayoutInflater mLayoutInflater;
        private final String meetingId;
        ArchivedMeetingActivity thisReference;

        List<String> ratedIds;
        List<String> myRatings;

        PlayersAdapter(Context context, List<String> members, List<String> ids, String meetingId,
                       ArchivedMeetingActivity t, List<String> ratedIds, List<String> myRatings)
        {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.mMembers = members;
            this.mIds = ids;
            this.meetingId = meetingId;
            this.thisReference = t;
            this.ratedIds = ratedIds;
            this.myRatings = myRatings;
        }

        @NonNull
        @Override
        public ArchivedMeetingActivity.PlayersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = mLayoutInflater.inflate(R.layout.game_history_details_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ArchivedMeetingActivity.PlayersAdapter.ViewHolder holder, int position)
        {
            String member = mMembers.get(position);
            String id = mIds.get(position);

            holder.name.setText(member);
            holder.userId = id;
            holder.meetingId = meetingId;
            holder.thisReference = thisReference;

            if (ratedIds.contains(id))
            {
                String mark = myRatings.get(ratedIds.indexOf(id));

                if (mark.equals("like"))
                {
                    holder.like.setColorFilter(Color.parseColor("#FF2879FA"));

                    holder.bLike = true;
                }
                else
                if (mark.equals("dislike"))
                {
                    holder.dislike.setColorFilter(Color.parseColor("#FF2879FA"));

                    holder.bDislike = true;
                }
            }
        }

        @Override
        public int getItemCount()
        {
            return mMembers.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder
        {
            private final FirebaseAuth mAuthenticator;

            public TextView name;
            public String userId;
            public String meetingId;
            public ImageButton like;
            public ImageButton dislike;
            public ArchivedMeetingActivity thisReference;

            private Boolean bLike, bDislike;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);

                mAuthenticator = FirebaseAuth.getInstance();
                bLike = false;
                bDislike = false;

                name = itemView.findViewById(R.id.player);
                like = itemView.findViewById(R.id.like_button);
                dislike = itemView.findViewById(R.id.dislike_button);

                like.setOnClickListener(view ->
                {
                    if (!bLike)
                    {
                        rate("like");

                        dislike.setColorFilter(Color.parseColor("#FFFFFFFF"));
                        like.setColorFilter(Color.parseColor("#FF2879FA"));

                        bLike = true;
                        bDislike = false;
                    }
                    else
                    {
                        unrate();

                        like.setColorFilter(Color.parseColor("#FFFFFFFF"));

                        bLike = false;
                        bLike = false;
                    }

                });

                dislike.setOnClickListener(view ->
                {
                    if (!bDislike)
                    {
                        rate("dislike");

                        like.setColorFilter(Color.parseColor("#FFFFFFFF"));
                        dislike.setColorFilter(Color.parseColor("#FF2879FA"));

                        bDislike = true;
                    }
                    else
                    {
                        unrate();

                        dislike.setColorFilter(Color.parseColor("#FFFFFFFF"));

                        bDislike = false;
                    }
                    bLike = false;
                });
            }

            private void rate(String rating)
            {
                mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
                {
                    if (task.isSuccessful())
                    {
                        String token = task.getResult().getToken();

                        UsersService usersService = Client.getClient(token).create(UsersService.class);

                        Call<Void> rateUser = usersService.rateUser(meetingId, userId, rating);

                        rateUser.enqueue(new Callback<Void>()
                        {
                            @Override
                            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {}

                            @Override
                            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t)
                            {
                                mAuthenticator.signOut();

                                ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
                            }
                        });
                    }
                    else
                    {
                        mAuthenticator.signOut();

                        ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
                    }
                });
            }

            private void unrate()
            {
                FirebaseAuth.getInstance().getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
                {
                    if (task.isSuccessful())
                    {
                        String token = task.getResult().getToken();

                        UsersService usersService = Client.getClient(token).create(UsersService.class);

                        Call<Void> unrate = usersService.unrateUser(meetingId, userId);

                        unrate.enqueue(new Callback<Void>()
                        {
                            @Override
                            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {}

                            @Override
                            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t)
                            {
                                mAuthenticator.signOut();

                                ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
                            }
                        });
                    }
                    else
                    {
                        mAuthenticator.signOut();

                        ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
                    }
                });
            }
        }
    }
}