package com.timewasters.tabletool.main.meeting.chat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.GenericTypeIndicator;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.main.meeting.MeetingActivity;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends AppCompatActivity
{
    private Chat chat;
    private List<Message> messages;

    private RecyclerView messageList;

    private EditText messageBox;
    private Button sendMessageButton;

    private ImageButton backButton;
    private ImageButton moreOptButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_chat_page);

        AndroidThreeTen.init(this);

        Bundle info = getIntent().getExtras();

        chat = new Chat(info.getString("meetingId"));
        messages = new ArrayList<>();

        initializeLayoutVariables();

        chat.setOnChangeListener((snapshot) ->
        {
            Message receivedMessage = snapshot.getValue(Message.class);

            if (receivedMessage != null && !receivedMessage.sender.equals(Static.nick))
            {
                messages.add(receivedMessage);

                displayMessages();
            }
        });

        sendMessageButton.setOnClickListener(view ->
        {
            String messageText = messageBox.getText().toString();

            if (!messageText.isEmpty())
            {
                LocalDateTime time = LocalDateTime.now();

                DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd MMMM");
                DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
                Message message = new Message(messageText, Static.nick, time.format(dateFormatter), time.format(timeFormatter));

                chat.write(message);

                messages.add(message);

                messageBox.getText().clear();

                displayMessages();
            }
        });

        backButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, MeetingActivity.class, info));

        messageList.setLayoutManager(new LinearLayoutManager(this));

        chat.getAllMessages(data ->
        {
            GenericTypeIndicator<List<Message>> t = new GenericTypeIndicator<List<Message>>() {};

            messages.clear();

            for (DataSnapshot d : data.getChildren())
                messages.add(d.getValue(Message.class));

            displayMessages();
        });
    }

    void initializeLayoutVariables()
    {
        messageList = findViewById(R.id.recycler_gchat);

        messageBox = findViewById(R.id.edit_gchat_message);
        sendMessageButton = findViewById(R.id.button_gchat_send);

        backButton = findViewById(R.id.back_button);
        moreOptButton = findViewById(R.id.more_opt_but);
    }

    void displayMessages()
    {
        MessagesAdapter adapter = new MessagesAdapter(this, messages);
        messageList.setAdapter(adapter);
    }

    static class MessagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        private final List<AdaptedMessage> mMessages;
        private final LayoutInflater mLayoutInflater;

        MessagesAdapter(Context context, List<Message> messages)
        {
            this.mLayoutInflater = LayoutInflater.from(context);

            this.mMessages = new ArrayList<>();

            if (messages != null)
            {
                for (Message m : messages)
                {
                    int viewType = m.sender.equals(Static.nick) ? 0 : 1;

                    this.mMessages.add(new AdaptedMessage(m, viewType));
                }
            }
        }

        @Override
        public int getItemViewType(int position)
        {
            return mMessages.get(position).viewType;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view;

            if (viewType == 0)
            {
                view = mLayoutInflater.inflate(R.layout.game_chat_my_messages, parent, false);

                return new MeViewHolder(view);
            }
            else
            {
                view = mLayoutInflater.inflate(R.layout.game_chat_other_messages, parent, false);

                return new OtherViewHolder(view);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
        {
            Message message = mMessages.get(position).message;

            if (mMessages.get(position).viewType == 1)
            {
                OtherViewHolder h = (OtherViewHolder)holder;

                h.nick.setText(message.sender);
                h.messageText.setText(message.message);
                h.date.setText(message.date);
                h.time.setText(message.time);
            }
            else
            {
                MeViewHolder h = (MeViewHolder)holder;

                h.messageText.setText(message.message);
                h.date.setText(message.date);
                h.time.setText(message.time);
            }
        }

        @Override
        public int getItemCount()
        {
            return mMessages.size();
        }

        static class OtherViewHolder extends RecyclerView.ViewHolder
        {
            public TextView nick;
            public TextView messageText;
            public TextView date;
            public TextView time;

            public OtherViewHolder(@NonNull View itemView)
            {
                super(itemView);

                nick = itemView.findViewById(R.id.text_gchat_user_other);
                messageText = itemView.findViewById(R.id.text_gchat_message);
                date = itemView.findViewById(R.id.text_gchat_date_other);
                time = itemView.findViewById(R.id.text_gchat_timestamp_other);
            }
        }

        static class MeViewHolder extends RecyclerView.ViewHolder
        {
            public TextView messageText;
            public TextView date;
            public TextView time;

            public MeViewHolder(@NonNull View itemView)
            {
                super(itemView);

                messageText = itemView.findViewById(R.id.text_gchat_message_me);
                date = itemView.findViewById(R.id.text_gchat_date_me);
                time = itemView.findViewById(R.id.text_gchat_timestamp_me);
            }

        }
    }

    static class AdaptedMessage
    {
        public Message message;
        public int viewType;


        public AdaptedMessage(Message message, int viewType)
        {
            this.message = message;
            this.viewType = viewType;
        }
    }
}