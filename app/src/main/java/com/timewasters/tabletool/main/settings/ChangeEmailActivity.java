package com.timewasters.tabletool.main.settings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.MainActivity;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.error.AuthenticationError;

public class ChangeEmailActivity extends AppCompatActivity
{
    private FirebaseAuth mAuthenticator;

    private ImageButton previousScreenButton;

    private EditText newEmailText;
    private EditText passwordConfirmationText;
    private TextView errorText;

    private Button changeEmailButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_email_page);

        ChangeEmailActivity thisReference = this;
        mAuthenticator = FirebaseAuth.getInstance();

        initializeLayoutVariables();

        previousScreenButton.setOnClickListener((view ->
                ActivityStarter.startActivity(this, SettingsActivity.class)));

        changeEmailButton.setOnClickListener((view ->
        {
            if (newEmailText.getText().toString().isEmpty())
            {
                errorText.setText(AuthenticationError.EMPTY_EMAIL.toString());

                return;
            }

            if (passwordConfirmationText.getText().toString().isEmpty())
            {
                errorText.setText(AuthenticationError.EMPTY_PASSWORD_CONFIRMATION.toString());

                return;
            }

            FirebaseUser user = mAuthenticator.getCurrentUser();

            AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(), passwordConfirmationText.getText().toString());

            user.reauthenticate(credential).addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                {
                    user.updateEmail(newEmailText.getText().toString()).addOnCompleteListener(task1 ->
                    {
                       if (task1.isSuccessful())
                           ActivityStarter.startActivity(thisReference, MainActivity.class);
                       else
                            errorText.setText(AuthenticationError.EMAIL_UPDATE_FAILED.toString());

                        clearFields();
                    });
                }
                else
                {
                    errorText.setText(AuthenticationError.WRONG_PASSWORD.toString());

                    clearFields();
                }
            });
        }));
    }

    private void initializeLayoutVariables()
    {
        previousScreenButton = findViewById(R.id.back_button);
        newEmailText = findViewById(R.id.new_email_field);
        passwordConfirmationText = findViewById(R.id.pass_conf_field);
        errorText = findViewById(R.id.error_text_block4);
        changeEmailButton = findViewById(R.id.change_email_button);
    }

    private void clearFields()
    {
        newEmailText.getText().clear();
        passwordConfirmationText.getText().clear();
    }
}