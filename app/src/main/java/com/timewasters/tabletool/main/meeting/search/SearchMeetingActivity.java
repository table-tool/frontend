package com.timewasters.tabletool.main.meeting.search;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.main.meeting.MeetingMembersActivity;
import com.timewasters.tabletool.service.UsersService;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchMeetingActivity extends AppCompatActivity
{
    private FirebaseAuth mAuthenticator;
    private FirebaseAnalytics mAnalytics;

    private TextView name;
    private TextView playersCount;
    private TextView date;
    private TextView time;
    private TextView address;
    private TextView city;
    private TextView description;

    private Button openPlayersScreenButton;
    private Button joinButton;

    private ImageButton backButton;
    private ImageButton moreOptionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_search_details_page);

        mAuthenticator = FirebaseAuth.getInstance();
        mAnalytics = FirebaseAnalytics.getInstance(this);

        initializeLayoutVariables();

        initializePage();

        Bundle extras = getIntent().getExtras();

        moreOptionsButton.setOnClickListener(view ->
                Static.showPopup(view, this));

        openPlayersScreenButton.setOnClickListener(view ->
        {
            extras.putString("search", null);

            ActivityStarter.startActivity(this, MeetingMembersActivity.class, extras);
        });

        backButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, MeetingsSearchActivity.class, extras));

        joinButton.setOnClickListener(view ->
                join(extras.getString("meetingId")));

        List<String> memberIds = extras.getStringArrayList("membersIds");

        if (memberIds.contains(mAuthenticator.getCurrentUser().getUid()))
        {
            joinButton.setActivated(false);
            joinButton.setVisibility(View.INVISIBLE);
        }
    }

    void initializeLayoutVariables()
    {
        name = findViewById(R.id.game_name);
        playersCount = findViewById(R.id.players_amount);
        date = findViewById(R.id.date_label);
        time = findViewById(R.id.time_label);
        address = findViewById(R.id.address_label);
        city = findViewById(R.id.city_label);
        description = findViewById(R.id.game_desc_textfield);

        openPlayersScreenButton = findViewById(R.id.button7);
        joinButton = findViewById(R.id.join_button);

        backButton = findViewById(R.id.back_button);
        moreOptionsButton = findViewById(R.id.more_opt_but);
    }

    void initializePage()
    {
        Bundle info = getIntent().getExtras();

        name.setText(info.getString("name"));
        playersCount.setText(info.getString("playerCount") + "/" + info.getString("maxPlayerCount") + " участников");

        String [] fullDate = info.getString("date").split("T");
        date.setText(fullDate[0]);
        time.setText(fullDate[1]);

        String addr = info.getString("address");
        int commaIndex = addr.indexOf(",");
        String lCity = addr.substring(0, commaIndex);
        String lAddr = addr.substring(commaIndex + 2);

        address.setText(lAddr);
        city.setText(lCity);

        description.setText(info.getString("description"));
    }

    void join(String meetingId)
    {
        SearchMeetingActivity thisReference = this;

        mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                String token = task.getResult().getToken();

                UsersService usersService = Client.getClient(token).create(UsersService.class);

                Call<Void> addToMeeting = usersService.addToMeeting(mAuthenticator.getCurrentUser().getUid(), meetingId);

                addToMeeting.enqueue(new Callback<Void>()
                {
                    @Override
                    public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response)
                    {
                        if (response.isSuccessful())
                        {
                            Toast.makeText(thisReference, "Вы успешно присоединились к встрече", Toast.LENGTH_LONG).show();

                            joinButton.setActivated(false);
                            joinButton.setVisibility(View.INVISIBLE);

                            Bundle event = new Bundle();
                            event.putString("joined", "true");
                            mAnalytics.logEvent("join_event", event);
                        }
                        else
                        {
                            Toast.makeText(thisReference, "Не удалось присоединиться к встрече", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t)
                    {
                        Static.signOut(thisReference);
                    }
                });
            }
            else
            {
                Static.signOut(thisReference);
            }
        });
    }
}