package com.timewasters.tabletool.main.meeting;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.main.meeting.chat.ChatActivity;

public class MeetingActivity extends AppCompatActivity
{
    private TextView name;
    private TextView playersCount;
    private TextView date;
    private TextView time;
    private TextView address;
    private TextView city;
    private TextView description;

    private Button openPlayersScreenButton;
    private Button goEditMeetingButton;
    private Button enterChatButton;

    private ImageButton backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_games_details_page);

        FirebaseAuth mAuthenticator = FirebaseAuth.getInstance();

        initializeLayoutVariables();

        initializePage();

        openPlayersScreenButton.setOnClickListener(view ->
        {
            Bundle extras = getIntent().getExtras();

            ActivityStarter.startActivity(this, MeetingMembersActivity.class, extras);
        });

        Bundle info = getIntent().getExtras();

        if (!info.getString("organizerId").equals(mAuthenticator.getCurrentUser().getUid()))
        {
            goEditMeetingButton.setEnabled(false);
            goEditMeetingButton.setVisibility(View.INVISIBLE);
        }

        goEditMeetingButton.setOnClickListener(view ->
        {
            Bundle extras = getIntent().getExtras();

            ActivityStarter.startActivity(this, EditMeetingActivity.class, extras);
        });

        enterChatButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, ChatActivity.class, info));

        if (info.containsKey("backToTheFuture"))
            backButton.setOnClickListener(view ->
                    ActivityStarter.startActivity(this, PlannedMeetingsActivity.class));
        else
            backButton.setOnClickListener(view ->
                    ActivityStarter.startActivity(this, MyMeetingsActivity.class));
    }

    void initializeLayoutVariables()
    {
        name = findViewById(R.id.game_name);
        playersCount = findViewById(R.id.players_amount);
        date = findViewById(R.id.date_label);
        time = findViewById(R.id.time_label);
        address = findViewById(R.id.address_label);
        city = findViewById(R.id.city_label);
        description = findViewById(R.id.game_desc_textfield);

        openPlayersScreenButton = findViewById(R.id.look_players);
        goEditMeetingButton = findViewById(R.id.change_game);
        enterChatButton = findViewById(R.id.chat_button);

        backButton = findViewById(R.id.back_button);
    }

    void initializePage()
    {
        Bundle info = getIntent().getExtras();

        name.setText(info.getString("name"));
        playersCount.setText(info.getString("playerCount") + "/" + info.getString("maxPlayerCount") + " участников");

        String [] fullDate = info.getString("date").split("T");
        date.setText(fullDate[0]);
        time.setText(fullDate[1]);

        String addr = info.getString("address");
        int commaIndex = addr.indexOf(",");
        String lCity = addr.substring(0, commaIndex);
        String lAddr = addr.substring(commaIndex + 2);

        address.setText(lAddr);
        city.setText(lCity);

        description.setText(info.getString("description"));
    }
}