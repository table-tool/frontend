package com.timewasters.tabletool.main.meeting.search;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.android.material.slider.RangeSlider;
import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.WelcomeActivity;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.service.GamesService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterActivity extends AppCompatActivity
{
    private FirebaseAuth mAuthenticator;

    private RecyclerView categorieList;

    private RangeSlider ratingSlider;
    private TextView minRatingText;
    private TextView maxRatingText;

    private AutoCompleteTextView city;

    private Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_sheet);

        mAuthenticator = FirebaseAuth.getInstance();

        initializeLayoutVariables();

        if (Static.city != null)
            city.setText(Static.city);

        if (Static.filterCity != null)
            city.setText(Static.filterCity);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.dropdown_menu_popup_item, Static.cities);

        city.setAdapter(adapter);

        minRatingText.setText(Static.ratings.get(0));
        maxRatingText.setText(Static.ratings.get(1));

        ratingSlider.setValueFrom(0.0f);
        ratingSlider.setValueTo(4.0f);
        ratingSlider.setValues(Float.parseFloat(Static.ratings.get(0)), Float.parseFloat(Static.ratings.get(1)));

        ratingSlider.addOnSliderTouchListener(new RangeSlider.OnSliderTouchListener()
        {
            @SuppressLint("RestrictedApi")
            @Override
            public void onStartTrackingTouch(@NonNull RangeSlider slider) {}

            @SuppressLint("RestrictedApi")
            @Override
            public void onStopTrackingTouch(@NonNull RangeSlider slider)
            {
                List<Float> values = slider.getValues();
                minRatingText.setText(values.get(0).intValue() + "");
                maxRatingText.setText(values.get(1).intValue() + "");
            }
        });

        submitButton.setOnClickListener(view ->
        {
            Static.filterCity = city.getText().toString();

            List<Float> values = ratingSlider.getValues();
            Static.ratings.set(0, values.get(0).intValue() + "");
            Static.ratings.set(1, values.get(1).intValue() + "");

            ActivityStarter.startActivity(this, MeetingsSearchActivity.class);
        });

        FilterActivity thisReference = this;

        mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                String token = task.getResult().getToken();

                GamesService gamesService = Client.getClient(token).create(GamesService.class);

                Call<List<String>> allCategories = gamesService.allCategories();

                allCategories.enqueue(new Callback<List<String>>()
                {
                    @Override
                    public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response)
                    {
                        if (response.isSuccessful())
                        {
                            categorieList.setLayoutManager(new LinearLayoutManager(thisReference));
                            CategoriesAdapter adapter = new CategoriesAdapter(thisReference, response.body());
                            categorieList.setAdapter(adapter);
                        }
                        else
                        {
                            ActivityStarter.startActivity(thisReference, MeetingsSearchActivity.class);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<String>> call, @NonNull Throwable t)
                    {
                        mAuthenticator.signOut();

                        ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
                    }
                });
            }
            else
            {
                mAuthenticator.signOut();

                ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
            }
        });
    }

    void initializeLayoutVariables()
    {
        categorieList = findViewById(R.id.game_types_list);

        ratingSlider = findViewById(R.id.creator_rating_slider);
        minRatingText = findViewById(R.id.min_rating);
        maxRatingText = findViewById(R.id.max_rating);

        city = findViewById(R.id.typesFilter);

        submitButton = findViewById(R.id.button);
    }

    static class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder>
    {
        private final List<String> categories;
        private final LayoutInflater mLayoutInflater;

        CategoriesAdapter(Context context, List<String> categories)
        {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.categories = categories;
        }

        @NonNull
        @Override
        public CategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = mLayoutInflater.inflate(R.layout.game_type_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CategoriesAdapter.ViewHolder holder, int position)
        {
            String categorie = categories.get(position);

            holder.name.setText(categorie);

            if (Static.categories.contains(categorie))
                holder.name.setChecked(true);
        }

        @Override
        public int getItemCount()
        {
            return categories.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder
        {
            public CheckBox name;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);

                name = itemView.findViewById(R.id.checkBox);

                name.setOnClickListener(view ->
                {
                    String categorie = name.getText().toString();

                    if (Static.categories.contains(categorie))
                        Static.categories.remove(categorie);
                    else
                        Static.categories.add(categorie);
                });
            }
        }
    }
}