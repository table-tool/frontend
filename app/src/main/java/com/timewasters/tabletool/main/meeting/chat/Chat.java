package com.timewasters.tabletool.main.meeting.chat;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

public class Chat
{
    private final FirebaseDatabase database;
    private final String id;

    Chat(String id)
    {
        this.database = FirebaseDatabase.getInstance();

        this.id = id;
    }

    public void write(Message message)
    {
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        database.getReference("chats").child(id).push().setValue(message);
    }

    public void getAllMessages(Listener listener)
    {
        database.getReference("chats").child(id).get().addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                listener.listen(task.getResult());
            }
        });
    }

    public void setOnChangeListener(Listener listener)
    {
        database.getReference("chats").child(id).addChildEventListener(new ChildEventListener()
        {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName)
            {
                listener.listen(snapshot);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {}

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {}

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {}

            @Override
            public void onCancelled(@NonNull DatabaseError error) {}
        });
    }

    interface Listener
    {
        void listen(@NonNull DataSnapshot snapshot);
    }
}
