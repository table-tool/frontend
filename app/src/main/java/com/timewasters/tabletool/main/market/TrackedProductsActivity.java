package com.timewasters.tabletool.main.market;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.MainActivity;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.service.ProductService;
import com.timewasters.tabletool.service.dto.product.ProductDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackedProductsActivity extends AppCompatActivity
{
    private FirebaseAuth mAuthenticator;
    private FirebaseAnalytics mAnalytics;

    private RecyclerView productList;
    private List<ProductDTO> products;

    private ImageButton backButton;
    private ImageButton moreOptionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorite_items_page);

        initializeLayoutVariables();

        mAuthenticator = FirebaseAuth.getInstance();
        mAnalytics = FirebaseAnalytics.getInstance(this);

        moreOptionsButton.setOnClickListener(view ->
                Static.showPopup(view, this));

        backButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, MainActivity.class));

        createProductList();
    }

    private void initializeLayoutVariables()
    {
        productList = findViewById(R.id.fav_list);

        moreOptionsButton = findViewById(R.id.more_opt_but);
        backButton = findViewById(R.id.back_button);
    }

    private void createProductList()
    {
        final TrackedProductsActivity thisReference = this;

        productList.setLayoutManager(new LinearLayoutManager(this));

        mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                String token = task.getResult().getToken();

                ProductService productService = Client.getClient(token).create(ProductService.class);

                Call<List<ProductDTO>> trackedProducts = productService.trackedProducts();

                trackedProducts.enqueue(new Callback<List<ProductDTO>>()
                {
                    @Override
                    public void onResponse(@NonNull Call<List<ProductDTO>> call, @NonNull Response<List<ProductDTO>> response)
                    {
                        if (response.isSuccessful())
                        {
                            products = response.body();

                            ProductsAdapter adapter = new ProductsAdapter(thisReference, products, thisReference);
                            productList.setAdapter(adapter);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<ProductDTO>> call, @NonNull Throwable t)
                    {
                        Static.signOut(thisReference);
                    }
                });
            }
            else
            {
                Static.signOut(thisReference);
            }
        });
    }

    static class ProductsAdapter extends RecyclerView.Adapter<TrackedProductsActivity.ProductsAdapter.ViewHolder>
    {
        private final List<ProductDTO> products;
        private final LayoutInflater mLayoutInflater;
        private final TrackedProductsActivity thisReference;

        ProductsAdapter(Context context, List<ProductDTO> products, TrackedProductsActivity thisReference)
        {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.products = products;
            this.thisReference = thisReference;
        }

        @NonNull
        @Override
        public TrackedProductsActivity.ProductsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = mLayoutInflater.inflate(R.layout.item_info_item, parent, false);
            return new TrackedProductsActivity.ProductsAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull TrackedProductsActivity.ProductsAdapter.ViewHolder holder, int position)
        {
            ProductDTO product = products.get(position);

            holder.name.setText(product.name);
            holder.store.setText("Ozon\n" + product.price);
            holder.goToStoreButton.setOnClickListener(view ->
            {
                Bundle event = new Bundle();
                event.putString("pushed", "true");
                thisReference.mAnalytics.logEvent("go_to_store_event", event);

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(product.link));
                thisReference.startActivity(browserIntent);
            });
        }

        @Override
        public int getItemCount()
        {
            return products.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder
        {
            public TextView name;
            public TextView store;
            public Button goToStoreButton;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);

                name = itemView.findViewById(R.id.item_price);
                store = itemView.findViewById(R.id.store_name);
                goToStoreButton = itemView.findViewById(R.id.go_to_store_button);
            }
        }
    }
}