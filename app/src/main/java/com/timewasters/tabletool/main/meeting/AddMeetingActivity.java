package com.timewasters.tabletool.main.meeting;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.error.MeetingError;
import com.timewasters.tabletool.error.ServerError;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.service.GamesService;
import com.timewasters.tabletool.service.MeetingService;
import com.timewasters.tabletool.service.dto.game.GameDTO;
import com.timewasters.tabletool.service.dto.meeting.CreateMeetingDTO;
import com.timewasters.tabletool.service.dto.meeting.MeetingDTO;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.LocalTime;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMeetingActivity extends AppCompatActivity
{
    private FirebaseAuth mAuthenticator;
    private MeetingService mMeetingService;
    private GamesService mGamesService;

    private List<GameDTO> games;

    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private String date;
    private String time;

    private LocalDate localDate;
    private LocalTime localTime;

    private SeekBar meetingPlayerCapacityBar;

    private Button pickDateButton;
    private Button pickHourButton;

    private EditText meetingAddress;
    private EditText meetingDescriptionText;

    private Button addMeetingButton;
    private ImageButton backButton;

    private Integer meetingPlayerCapacity;

    private AutoCompleteTextView pickedGame;

    private TextView errorText;
    private TextView pickedPlayerCount;

    private ImageButton moreOptionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_create_page);

        AndroidThreeTen.init(this);

        meetingPlayerCapacity = 0;
        date = "";
        time = "";
        initializeLayoutVariables();

        AddMeetingActivity thisReference = this;
        mAuthenticator = FirebaseAuth.getInstance();

        initializeGameList();

        meetingPlayerCapacityBar.setMax(20);
        pickedPlayerCount.setText("0");

        moreOptionsButton.setOnClickListener(view ->
                Static.showPopup(view, this));

        meetingPlayerCapacityBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b)
            {
                meetingPlayerCapacity = seekBar.getProgress();

                pickedPlayerCount.setText(meetingPlayerCapacity + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        DatePickerDialog.OnDateSetListener listener = (datePicker, year, month, day) ->
        {
            localDate = LocalDate.of(year, month + 1, day);
            date = localDate.toString();

            pickDateButton.setText(date);
        };

        TimePickerDialog.OnTimeSetListener timeSetListener = (timePicker, hour, minute) ->
        {
            localTime = LocalTime.of(hour, minute);
            time = localTime.toString();

            pickHourButton.setText(time);
        };

        LocalDateTime d = LocalDateTime.now();

        datePickerDialog = new DatePickerDialog(this, listener, d.getYear(), d.getMonthValue() - 1, d.getDayOfMonth());
        timePickerDialog = new TimePickerDialog(this, timeSetListener, 12, 0, true);

        pickDateButton.setOnClickListener(view ->
                datePickerDialog.show());

        pickHourButton.setOnClickListener(view ->
                timePickerDialog.show());

        backButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, MyMeetingsActivity.class));

        addMeetingButton.setOnClickListener((view ->
        {
            if (localDate.compareTo(LocalDate.now()) < 0)
            {
                errorText.setText(MeetingError.TIME.toString());

                return;
            }

            if (localDate.compareTo(LocalDate.now()) == 0 && localTime.compareTo(LocalTime.now()) < 0)
            {
                errorText.setText(MeetingError.TIME.toString());

                return;
            }

            if (Static.city == null || Static.city.isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_CITY.toString());

                return;
            }

            if (pickedGame.getText().toString().isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_GAME.toString());

                return;
            }

            if (meetingPlayerCapacity == 0 || meetingPlayerCapacity == 1)
            {
                errorText.setText(MeetingError.ONE_MAN_ARMY.toString());

                return;
            }

            if (date.isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_DATE.toString());

                return;
            }

            if (time.isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_TIME.toString());

                return;
            }

            if (meetingAddress.getText().toString().isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_ADDRESS.toString());

                return;
            }

            if (meetingDescriptionText.getText().toString().isEmpty())
            {
                errorText.setText(MeetingError.EMPTY_DESCRIPTION.toString());

                return;
            }

            String fullDate = date + "T" + time;

            List<String> game = new ArrayList<>();
            game.add(pickedGame.getText().toString());

            mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                {
                    String token = task.getResult().getToken();

                    mMeetingService = Client.getClient(token).create(MeetingService.class);

                    CreateMeetingDTO data = new CreateMeetingDTO
                            (
                                    Static.city + ", " + meetingAddress.getText().toString(),
                                    meetingDescriptionText.getText().toString(),
                                    451,
                                    meetingPlayerCapacity,
                                    mAuthenticator.getCurrentUser().getUid(),
                                    "ripnites",
                                    fullDate,
                                    game,
                                    null
                            );

                    Call<MeetingDTO> createMeeting = mMeetingService.createMeeting(data);

                    createMeeting.enqueue(new Callback<MeetingDTO>()
                    {
                        @Override
                        public void onResponse(@NonNull Call<MeetingDTO> call, @NonNull Response<MeetingDTO> response)
                        {
                            if (response.isSuccessful())
                            {
                                ActivityStarter.startActivity(thisReference, MyMeetingsActivity.class);
                            }
                            else
                            {
                                errorText.setText(ServerError.SERVER_INTERNAL.toString());
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<MeetingDTO> call, @NonNull Throwable t)
                        {
                            errorText.setText(ServerError.SERVER_UNREACHABLE.toString());
                        }
                    });
                }
                else
                {
                    errorText.setText(ServerError.SERVER_UNREACHABLE.toString());
                }
            });
        }));
    }

    private void initializeLayoutVariables()
    {
        meetingPlayerCapacityBar = findViewById(R.id.seekBar);

        pickDateButton = findViewById(R.id.pick_date_button);
        pickHourButton = findViewById(R.id.pick_time_button);

        meetingAddress = findViewById(R.id.game_name);
        meetingDescriptionText = findViewById(R.id.game_desc_textfield);

        addMeetingButton = findViewById(R.id.create_game_button);
        backButton = findViewById(R.id.back_button);

        pickedGame = findViewById(R.id.typesFilter);

        errorText = findViewById(R.id.error_text_block);
        pickedPlayerCount = findViewById(R.id.chosen_amount);
        moreOptionsButton = findViewById(R.id.more_opt_but);
    }

    private void initializeGameList()
    {
        AddMeetingActivity thisReference = this;

        mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
        {
           if (task.isSuccessful())
           {
               String token = task.getResult().getToken();

               mGamesService = Client.getClient(token).create(GamesService.class);

               Call<List<GameDTO>> gamesList = mGamesService.allGames();

               gamesList.enqueue(new Callback<List<GameDTO>>()
               {
                   @Override
                   public void onResponse(@NonNull Call<List<GameDTO>> call, @NonNull Response<List<GameDTO>> response)
                   {
                       if (response.isSuccessful())
                       {
                           games = response.body();
                           ArrayList<String> gameNames = new ArrayList<>();

                           for (GameDTO game : games)
                           {
                               gameNames.add(game.name);
                           }

                           ArrayAdapter<String> adapter = new ArrayAdapter<>(thisReference, R.layout.dropdown_menu_popup_item, gameNames);

                           pickedGame.setAdapter(adapter);
                       }
                       else
                       {
                           errorText.setText(ServerError.SERVER_INTERNAL.toString());
                       }
                   }

                   @Override
                   public void onFailure(@NonNull Call<List<GameDTO>> call, @NonNull Throwable t)
                   {
                       errorText.setText(ServerError.SERVER_UNREACHABLE.toString());
                   }
               });
           }
           else
           {
               errorText.setText(ServerError.SERVER_UNREACHABLE.toString());
           }
        });
    }
}