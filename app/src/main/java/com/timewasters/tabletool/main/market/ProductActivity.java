package com.timewasters.tabletool.main.market;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.service.dto.product.ProductDTO;

import java.util.ArrayList;
import java.util.List;

public class ProductActivity extends AppCompatActivity
{
    private FirebaseAnalytics mAnalytics;

    private ImageView image;

    private RecyclerView shopList;

    private ImageButton moreOptionsButton;
    private ImageButton backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_info_page);

        initializeLayoutVariables();

        mAnalytics = FirebaseAnalytics.getInstance(this);

        Bundle info = getIntent().getExtras();
        ProductDTO product = new ProductDTO(info);

        moreOptionsButton.setOnClickListener(view ->
                Static.showPopup(view, this));

        backButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, MarketActivity.class, info));

        initializePage(product);

        createShopList(product);
    }

    private void initializeLayoutVariables()
    {
        image = findViewById(R.id.item_pic);

        shopList = findViewById(R.id.item_store_list);

        moreOptionsButton = findViewById(R.id.more_opt_but);
        backButton = findViewById(R.id.back_button);
    }

    private void initializePage(ProductDTO product)
    {
        image.setImageDrawable(Static.LoadImageFromWebOperations(product.imageLink));
    }

    private void createShopList(ProductDTO product)
    {
        ArrayList<ProductDTO> products = new ArrayList<>();
        products.add(product);

        shopList.setLayoutManager(new LinearLayoutManager(this));
        ShopAdapter adapter = new ShopAdapter(this, products, this);
        shopList.setAdapter(adapter);
    }

    static class ShopAdapter extends RecyclerView.Adapter<ProductActivity.ShopAdapter.ViewHolder>
    {
        private final List<ProductDTO> products;
        private final LayoutInflater mLayoutInflater;
        private final ProductActivity thisReference;

        ShopAdapter(Context context, List<ProductDTO> products, ProductActivity thisReference)
        {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.products = products;
            this.thisReference = thisReference;
        }

        @NonNull
        @Override
        public ProductActivity.ShopAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = mLayoutInflater.inflate(R.layout.item_info_item, parent, false);
            return new ProductActivity.ShopAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ProductActivity.ShopAdapter.ViewHolder holder, int position)
        {
           ProductDTO productDTO = products.get(position);

           holder.price.setText(productDTO.price + "");
           holder.store.setText("Ozon");
           holder.goToStoreButton.setOnClickListener(view ->
           {
               Bundle event = new Bundle();
               event.putString("pushed", "true");
               thisReference.mAnalytics.logEvent("go_to_store_event", event);

               Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(productDTO.link));
               thisReference.startActivity(browserIntent);
           });

            holder.image.setImageDrawable(Static.LoadImageFromWebOperations("https://static.tildacdn.com/tild3861-6339-4234-b534-616338663632/Frame_181.png"));
        }

        @Override
        public int getItemCount()
        {
            return products.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder
        {
            public ImageView image;
            public TextView price;
            public TextView store;
            public Button goToStoreButton;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);

                image = itemView.findViewById(R.id.store_pic);
                price = itemView.findViewById(R.id.item_price);
                store = itemView.findViewById(R.id.store_name);
                goToStoreButton = itemView.findViewById(R.id.go_to_store_button);
            }
        }
    }
}