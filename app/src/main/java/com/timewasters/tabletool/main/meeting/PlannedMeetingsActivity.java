package com.timewasters.tabletool.main.meeting;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.ItemClickListener;
import com.timewasters.tabletool.MainActivity;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.WelcomeActivity;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.service.MeetingService;
import com.timewasters.tabletool.service.dto.game.GameDTO;
import com.timewasters.tabletool.service.dto.meeting.MeetingDTO;
import com.timewasters.tabletool.service.dto.meeting.PageMeetingDTO;

import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlannedMeetingsActivity extends AppCompatActivity implements ItemClickListener
{
    private FirebaseAuth mAuthenticator;
    private MeetingService mMeetingService;

    private List<MeetingDTO> meetings;

    private RecyclerView meetingsList;
    private MeetingsAdapter adapter;

    private ImageButton previousScreenButton;
    private ImageButton moreOptionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_planned_page);

        mAuthenticator = FirebaseAuth.getInstance();

        initializeLayoutVariables();

        createMeetingList();

        previousScreenButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, MainActivity.class));

        moreOptionsButton.setOnClickListener(view ->
                Static.showPopup(view, this));
    }

    void initializeLayoutVariables()
    {
        previousScreenButton = findViewById(R.id.back_button);
        meetingsList = findViewById(R.id.planned_games_list);
        moreOptionsButton = findViewById(R.id.more_opt_but);
    }

    void createMeetingList()
    {
        PlannedMeetingsActivity thisReference = this;

        mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                String token = task.getResult().getToken();

                mMeetingService = Client.getClient(token).create(MeetingService.class);

                Call<PageMeetingDTO> usersMeetings = mMeetingService.usersMeetings(mAuthenticator.getCurrentUser().getUid(), "future", 1984, null);

                usersMeetings.enqueue(new Callback<PageMeetingDTO>()
                {
                    @Override
                    public void onResponse(@NonNull Call<PageMeetingDTO> call, @NonNull Response<PageMeetingDTO> response)
                    {
                        if (response.isSuccessful())
                        {
                            meetings = response.body().items;

                            meetingsList.setLayoutManager(new LinearLayoutManager(thisReference));
                            adapter = new PlannedMeetingsActivity.MeetingsAdapter(thisReference, meetings, thisReference);
                            adapter.setClickListener(thisReference);

                            meetingsList.setAdapter(adapter);
                        }
                        else
                        {
                            ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<PageMeetingDTO> call, @NonNull Throwable t)
                    {
                        mAuthenticator.signOut();

                        ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
                    }
                });
            }
            else
            {
                ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
            }
        });
    }

    @Override
    public void onItemClick(View view, int position)
    {
        MeetingDTO meeting = meetings.get(position);

        Bundle extras = meeting.pack();
        extras.putString("backToTheFuture", null);

        ActivityStarter.startActivity(this, MeetingActivity.class, extras);
    }

    static class MeetingsAdapter extends RecyclerView.Adapter<PlannedMeetingsActivity.MeetingsAdapter.ViewHolder>
    {
        private final List<MeetingDTO> mMeetings;
        private final LayoutInflater mLayoutInflater;
        private ItemClickListener mItemClickListener;
        private PlannedMeetingsActivity context;

        MeetingsAdapter(Context context, List<MeetingDTO> meetings, PlannedMeetingsActivity t)
        {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.mMeetings = meetings;
            this.context = t;
        }

        @NonNull
        @Override
        public PlannedMeetingsActivity.MeetingsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = mLayoutInflater.inflate(R.layout.game_plan_item, parent, false);
            return new PlannedMeetingsActivity.MeetingsAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull PlannedMeetingsActivity.MeetingsAdapter.ViewHolder holder, int position)
        {
            MeetingDTO meeting = mMeetings.get(position);

            holder.description = meeting.description;

            holder.context = this.context;

            String gameName = meeting.games.get(0);

            holder.name.setText(gameName);
            holder.address.setText(meeting.address);
            String [] date = meeting.startDate.split("T");
            holder.date.setText(date[0] + " " + date[1]);
            holder.startDate = date[1];
            holder.rating.setText(meeting.organizerRating);

            holder.addToCalendarButton.setOnClickListener(view ->
            {
                Intent intent = new Intent(Intent.ACTION_INSERT);
                intent.setData(CalendarContract.Events.CONTENT_URI);
                intent.putExtra(CalendarContract.Events.TITLE, holder.name.getText().toString());
                intent.putExtra(CalendarContract.Events.DESCRIPTION, holder.description);
                intent.putExtra(CalendarContract.Events.EVENT_LOCATION, holder.address.getText().toString());
                intent.putExtra(CalendarContract.Events.ALL_DAY, "false");
                intent.putExtra(CalendarContract.Events.DTSTART, holder.date.getText().toString());

                context.startActivity(intent);
            });

            List<GameDTO> encodedImage = Static.games.stream().filter(element -> element.name.equals(gameName)).collect(Collectors.toList());

            byte[] decodedString = Base64.decode(encodedImage.get(0).image, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            holder.picture.setImageBitmap(decodedByte);
        }

        @Override
        public int getItemCount()
        {
            return mMeetings.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {
            public TextView name;
            public TextView address;
            public TextView rating;
            public TextView date;
            public ImageView picture;
            public Button addToCalendarButton;

            public String description;
            public String startDate;

            public Context context;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);

                name = itemView.findViewById(R.id.game_name);
                address = itemView.findViewById(R.id.game_address);
                rating = itemView.findViewById(R.id.rating);
                date = itemView.findViewById(R.id.date);
                picture = itemView.findViewById(R.id.game_pic);
                addToCalendarButton = itemView.findViewById(R.id.add_to_calendar_button);



                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view)
            {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, getAdapterPosition());
            }
        }

        void setClickListener(ItemClickListener itemClickListener)
        {
            this.mItemClickListener = itemClickListener;
        }
    }
}