package com.timewasters.tabletool.main.settings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;

import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.main.Static;

public class SettingsActivity extends AppCompatActivity
{

    private ImageButton previousScreenButton;

    private Button changeEmailButton;
    private Button changeNicknameButton;
    private Button changePasswordButton;
    private Button logOutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_page);

        initializeLayoutVariables();

        previousScreenButton.setOnClickListener(view ->
                ActivityStarter.startMainActivity(this));

        changeEmailButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, ChangeEmailActivity.class));

        changeNicknameButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, ChangeNicknameActivity.class));

        changePasswordButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, ChangePasswordActivity.class));

        logOutButton.setOnClickListener((view ->
                Static.signOut(this)));
    }

    private void initializeLayoutVariables()
    {
        previousScreenButton = findViewById(R.id.back_button);
        changeEmailButton = findViewById(R.id.email_settings);
        changeNicknameButton = findViewById(R.id.nickname_settings);
        changePasswordButton = findViewById(R.id.pass_settings);
        logOutButton = findViewById(R.id.logout);
    }
}