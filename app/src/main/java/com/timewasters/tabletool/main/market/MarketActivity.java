package com.timewasters.tabletool.main.market;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.error.ServerError;
import com.timewasters.tabletool.main.Navigation;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.service.ProductService;
import com.timewasters.tabletool.service.dto.product.ProductDTO;
import com.timewasters.tabletool.service.dto.product.TrackedProductDTO;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarketActivity extends AppCompatActivity
{
    private FirebaseAuth mAuthenticator;
    private FirebaseAnalytics mAnalytics;

    private RecyclerView productList;
    private List<ProductDTO> products;
    private String query;

    private SearchView searchBar;

    private ImageButton moreOptionsButton;

    private Navigation navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.market_page);

        initializeLayoutVariables();

        mAuthenticator = FirebaseAuth.getInstance();
        mAnalytics = FirebaseAnalytics.getInstance(this);

        if (mAuthenticator.getCurrentUser() != null)
        {
            navigation = new Navigation(this, R.id.home);
        }

        productList.setLayoutManager(new LinearLayoutManager(this));

        MarketActivity thisReference = this;

        searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                thisReference.query = query;

                createProductList(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                return false;
            }
        });

        Bundle info = getIntent().getExtras();

        if (info != null && info.containsKey("query"))
        {
            query = info.getString("query");

            searchBar.setQuery(query, true);
        }

        moreOptionsButton.setOnClickListener(view ->
                Static.showPopup(view, this));
    }

    void initializeLayoutVariables()
    {
        productList = findViewById(R.id.faq_list);

        searchBar = findViewById(R.id.search_market);

        moreOptionsButton = findViewById(R.id.more_opt_but);
    }

    private void createProductList(String query)
    {
        MarketActivity thisReference = this;

        ProductService productService = Client.getClient().create(ProductService.class);

        Call<List<ProductDTO>> searchProducts = productService.searchProducts(query);

        searchProducts.enqueue(new Callback<List<ProductDTO>>()
        {
            @Override
            public void onResponse(@NonNull Call<List<ProductDTO>> call, @NonNull Response<List<ProductDTO>> response)
            {
                if (response.isSuccessful())
                {
                    products = response.body();

                    ProductsAdapter adapter = new ProductsAdapter(thisReference, products, thisReference);
                    productList.setAdapter(adapter);
                }
                else
                {
                    try {
                        System.out.println(response.errorBody().string());
                    } catch (IOException exception) {
                        exception.printStackTrace();
                    }
                    Toast.makeText(thisReference, ServerError.SERVER_UNREACHABLE.toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<ProductDTO>> call, @NonNull Throwable t)
            {
                Toast.makeText(thisReference, ServerError.SERVER_UNREACHABLE.toString(), Toast.LENGTH_LONG).show();
                Static.signOut(thisReference);
            }
        });
    }

    private void addToTrackable(ProductDTO product)
    {
        MarketActivity thisReference = this;

        mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                String token = task.getResult().getToken();

                ProductService productService = Client.getClient(token).create(ProductService.class);

                Call<TrackedProductDTO> addToTrackable = productService.addToTrackable(product.toTracked(mAuthenticator.getCurrentUser().getUid()));

                addToTrackable.enqueue(new Callback<TrackedProductDTO>()
                {
                    @Override
                    public void onResponse(@NonNull Call<TrackedProductDTO> call, @NonNull Response<TrackedProductDTO> response)
                    {
                        if (response.isSuccessful())
                        {
                            Toast.makeText(thisReference, "Товар добавлен в отслеживаемое", Toast.LENGTH_LONG).show();

                            Bundle event = new Bundle();
                            event.putString("added", "true");
                            mAnalytics.logEvent("product_tracking_event", event);
                        }
                        else
                        {
                            try {
                                System.out.println(response.errorBody().string());
                            } catch (IOException exception) {
                                exception.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<TrackedProductDTO> call, @NonNull Throwable t)
                    {
                        Toast.makeText(thisReference, ServerError.SERVER_UNREACHABLE.toString(), Toast.LENGTH_LONG).show();
                        Static.signOut(thisReference);
                    }
                });
            }
            else
            {
                Toast.makeText(thisReference, ServerError.SERVER_UNREACHABLE.toString(), Toast.LENGTH_LONG).show();
                Static.signOut(thisReference);
            }
        });
    }

    static class ProductsAdapter extends RecyclerView.Adapter<MarketActivity.ProductsAdapter.ViewHolder>
    {
        private final List<ProductDTO> products;
        private final LayoutInflater mLayoutInflater;
        private final MarketActivity thisReference;

        ProductsAdapter(Context context, List<ProductDTO> products, MarketActivity thisReference)
        {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.products = products;
            this.thisReference = thisReference;
        }

        @NonNull
        @Override
        public MarketActivity.ProductsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = mLayoutInflater.inflate(R.layout.market_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MarketActivity.ProductsAdapter.ViewHolder holder, int position)
        {
            ProductDTO product = products.get(position);

            if (product.name.length() > 22)
                holder.name.setText(product.name.substring(0, 22) + "...");
            else
                holder.name.setText(product.name);

            holder.description.setText(product.price + "");
            holder.image.setImageDrawable(Static.LoadImageFromWebOperations(product.imageLink));

            if (thisReference.mAuthenticator.getCurrentUser() != null)
            {
                holder.addToTrackable.setOnClickListener(view ->
                {
                    thisReference.addToTrackable(product);
                    holder.addToTrackable.setColorFilter(Color.parseColor("#FF2879FA"));
                });
            }
            else
            {
                holder.addToTrackable.setEnabled(false);
                holder.addToTrackable.setVisibility(View.INVISIBLE);
            }


            holder.openDetails.setOnClickListener(view ->
            {
                Bundle extras = product.pack();

                extras.putString("query", thisReference.query);

                ActivityStarter.startActivity(thisReference, ProductActivity.class, extras);
            });
        }

        @Override
        public int getItemCount()
        {
            return products.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder
        {
            public TextView name;
            public TextView description;
            public ImageView image;

            public ImageButton addToTrackable;
            public Button openDetails;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);

                name = itemView.findViewById(R.id.game_name);
                description = itemView.findViewById(R.id.game_desc);
                image = itemView.findViewById(R.id.item_pic);

                addToTrackable = itemView.findViewById(R.id.add_to_fav_button);
                openDetails = itemView.findViewById(R.id.more_info_button);
            }
        }
    }
}