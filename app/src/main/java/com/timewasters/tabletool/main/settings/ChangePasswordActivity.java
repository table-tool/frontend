package com.timewasters.tabletool.main.settings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.MainActivity;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.error.AuthenticationError;
import com.timewasters.tabletool.error.ServerError;

public class ChangePasswordActivity extends AppCompatActivity
{
    private FirebaseAuth mAuthenticator;

    private ImageButton previousScreenButton;

    private EditText passwordConfirmationText;
    private EditText newPasswordText;
    private EditText newPasswordConfirmationText;
    private TextView errorText;

    private Button changePasswordButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_password_page);

        mAuthenticator = FirebaseAuth.getInstance();
        ChangePasswordActivity thisReference = this;

        initializeLayoutVariables();

        previousScreenButton.setOnClickListener((view ->
                ActivityStarter.startActivity(this, SettingsActivity.class)));

        changePasswordButton.setOnClickListener((view ->
        {
            if (passwordConfirmationText.getText().toString().isEmpty())
            {
                errorText.setText(AuthenticationError.EMPTY_PASSWORD_CONFIRMATION.toString());

                return;
            }

            if (newPasswordText.getText().toString().isEmpty())
            {
                errorText.setText(AuthenticationError.EMPTY_PASSWORD.toString());

                return;
            }

            if (newPasswordConfirmationText.getText().toString().isEmpty())
            {
                errorText.setText(AuthenticationError.EMPTY_PASSWORD.toString());

                return;
            }

            if (!newPasswordText.getText().toString().equals(newPasswordConfirmationText.getText().toString()))
            {
                errorText.setText(AuthenticationError.PASSWORDS_DO_NOT_MATCH.toString());

                return;
            }

            FirebaseUser user = mAuthenticator.getCurrentUser();

            AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(), passwordConfirmationText.getText().toString());

            System.out.println(newPasswordText.getText().toString());

            user.reauthenticate(credential).addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                {
                    user.updatePassword(newPasswordText.getText().toString()).addOnCompleteListener(task1 ->
                    {
                        if (task1.isSuccessful())
                            ActivityStarter.startActivity(thisReference, MainActivity.class);
                        else
                            errorText.setText(ServerError.WRONG_INPUT.toString());

                        clearFields();
                    });
                }
                else
                {
                    errorText.setText(AuthenticationError.WRONG_PASSWORD.toString());

                    clearFields();
                }
            });
        }));
    }

    private void initializeLayoutVariables()
    {
        previousScreenButton = findViewById(R.id.back_button);
        passwordConfirmationText = findViewById(R.id.old_pass_field);
        newPasswordText = findViewById(R.id.new_pass_field);
        newPasswordConfirmationText = findViewById(R.id.confirm_new_pass_field);
        errorText = findViewById(R.id.error_text_block5);
        changePasswordButton = findViewById(R.id.change_password_button);
    }

    private void clearFields()
    {
        passwordConfirmationText.getText().clear();
        newPasswordText.getText().clear();
        newPasswordConfirmationText.getText().clear();
    }
}