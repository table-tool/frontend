package com.timewasters.tabletool.main.meeting.search;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.ItemClickListener;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.WelcomeActivity;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.main.Navigation;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.main.meeting.AddMeetingActivity;
import com.timewasters.tabletool.service.MeetingService;
import com.timewasters.tabletool.service.dto.game.GameDTO;
import com.timewasters.tabletool.service.dto.meeting.MeetingDTO;
import com.timewasters.tabletool.service.dto.meeting.PageMeetingDTO;

import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeetingsSearchActivity extends AppCompatActivity implements ItemClickListener
{
    private FirebaseAuth mAuthenticator;
    private MeetingService mMeetingService;

    private List<MeetingDTO> meetings;

    private SearchView searchBar;
    private MeetingsAdapter adapter;

    private RecyclerView meetingsList;

    private ImageButton addButton;
    private ImageButton filterButton;
    private ImageButton moreOptionsButton;

    private Navigation navigation;

    private String lastQuery = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_search_page);

        mAuthenticator = FirebaseAuth.getInstance();

        navigation = new Navigation(this, R.id.settings);
        initializeLayoutVariables();

        meetingsList.setLayoutManager(new LinearLayoutManager(this));

        Bundle info = getIntent().getExtras();

        moreOptionsButton.setOnClickListener(view ->
                Static.showPopup(view, this));

        searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                createMeetingList(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                return false;
            }
        });

        if (info != null && info.containsKey("query") && info.getString("query") != null)
        {
            String query = info.getString("query");

            searchBar.setQuery(query, true);
        }

        addButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, AddMeetingActivity.class));

        filterButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, FilterActivity.class));
    }

    void createMeetingList(String query)
    {
        lastQuery = query;

        MeetingsSearchActivity thisReference = this;

        String city = Static.city;

        if (Static.filterCity != null)
            city = Static.filterCity;

        if (city == null)
            city = Static.cities.get(0);

        final String finalCity = city;
        final String finalQuery = query.isEmpty() ? null : query;
        mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                String token = task.getResult().getToken();

                mMeetingService = Client.getClient(token).create(MeetingService.class);

                Call<PageMeetingDTO> usersMeetings = mMeetingService
                        .usersMeetings(null,
                                "future",
                                1984,
                                null,
                                Static.categories.isEmpty() ? null : Static.categories,
                                finalCity,
                                Static.ratings.get(0),
                                Static.ratings.get(1),
                                finalQuery);

                usersMeetings.enqueue(new Callback<PageMeetingDTO>()
                {
                    @Override
                    public void onResponse(@NonNull Call<PageMeetingDTO> call, @NonNull Response<PageMeetingDTO> response)
                    {
                        if (response.isSuccessful())
                        {
                            meetings = response.body().items;

                            meetings = meetings.stream().filter(element -> !element.organizerId.equals(mAuthenticator.getCurrentUser().getUid()))
                                    .collect(Collectors.toList());

                            meetingsList.setLayoutManager(new LinearLayoutManager(thisReference));
                            adapter = new MeetingsSearchActivity.MeetingsAdapter(thisReference, meetings);
                            adapter.setClickListener(thisReference);

                            meetingsList.setAdapter(adapter);
                        }
                        else
                        {
                            mAuthenticator.signOut();

                            ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<PageMeetingDTO> call, @NonNull Throwable t)
                    {
                        mAuthenticator.signOut();

                        ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
                    }
                });
            }
            else
            {
                mAuthenticator.signOut();

                ActivityStarter.startActivity(thisReference, WelcomeActivity.class);
            }
        });
    }

    void initializeLayoutVariables()
    {
        searchBar = findViewById(R.id.search_games);

        meetingsList = findViewById(R.id.game_search_list);

        addButton = findViewById(R.id.add_button);
        filterButton = findViewById(R.id.filter_button);
        moreOptionsButton = findViewById(R.id.more_opt_but);
    }

    @Override
    public void onItemClick(View view, int position)
    {
        MeetingDTO pickedMeeting = meetings.get(position);

        Bundle extras = pickedMeeting.pack();
        extras.putString("query", lastQuery);

        ActivityStarter.startActivity(this, SearchMeetingActivity.class, extras);
    }

    static class MeetingsAdapter extends RecyclerView.Adapter<MeetingsSearchActivity.MeetingsAdapter.ViewHolder>
    {
        private final List<MeetingDTO> mMeetings;
        private final LayoutInflater mLayoutInflater;
        private ItemClickListener mItemClickListener;

        MeetingsAdapter(Context context, List<MeetingDTO> meetings)
        {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.mMeetings = meetings;
        }

        @NonNull
        @Override
        public MeetingsSearchActivity.MeetingsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = mLayoutInflater.inflate(R.layout.my_games_item, parent, false);
            return new MeetingsSearchActivity.MeetingsAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MeetingsSearchActivity.MeetingsAdapter.ViewHolder holder, int position)
        {
            MeetingDTO meeting = mMeetings.get(position);

            String gameName = meeting.games.get(0);

            holder.name.setText(gameName);
            holder.address.setText(meeting.address);
            String [] date = meeting.startDate.split("T");
            holder.date.setText(date[0] + " " + date[1]);
            holder.rating.setText(meeting.organizerRating);

            List<GameDTO> encodedImage = Static.games.stream().filter(element -> element.name.equals(gameName)).collect(Collectors.toList());

            byte[] decodedString = Base64.decode(encodedImage.get(0).image, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            holder.picture.setImageBitmap(decodedByte);
        }

        @Override
        public int getItemCount()
        {
            return mMeetings.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {
            public TextView name;
            public TextView address;
            public TextView rating;
            public TextView date;
            public ImageView picture;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);

                name = itemView.findViewById(R.id.game_name);
                address = itemView.findViewById(R.id.game_address);
                rating = itemView.findViewById(R.id.rating);
                date = itemView.findViewById(R.id.date);
                picture = itemView.findViewById(R.id.game_pic);

                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view)
            {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, getAdapterPosition());
            }
        }

        void setClickListener(ItemClickListener itemClickListener)
        {
            this.mItemClickListener = itemClickListener;
        }
    }
}