package com.timewasters.tabletool.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.MainActivity;
import com.timewasters.tabletool.R;

import java.util.ArrayList;
import java.util.List;

public class FAQActivity extends AppCompatActivity
{
    private ImageButton backButton;
    private RecyclerView faqList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq_page);

        backButton = findViewById(R.id.back_button);
        faqList = findViewById(R.id.faq_list);

        faqList.setLayoutManager(new LinearLayoutManager(this));

        ArrayList<Faq> faqs = new ArrayList<>();

        faqs.add(new Faq("Почему крепкий сон так сильно походит на смерть?", "Король себя сам к пастуху не приравняет"));
        faqs.add(new Faq("?", "..."));

        FAQAdapter adapter = new FAQAdapter(this, faqs);

        faqList.setAdapter(adapter);

        backButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, MainActivity.class));
    }

    static class Faq
    {
        public String question;
        public String answer;

        public Faq(String question, String answer)
        {
            this.question = question;
            this.answer = answer;
        }
    }

    static class FAQAdapter extends RecyclerView.Adapter<FAQAdapter.ViewHolder>
    {
        private final List<Faq> faqs;
        private final LayoutInflater mLayoutInflater;

        FAQAdapter(Context context, List<Faq> faqs)
        {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.faqs = faqs;
        }

        @NonNull
        @Override
        public FAQAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = mLayoutInflater.inflate(R.layout.faq_item, parent, false);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull FAQAdapter.ViewHolder holder, int position)
        {
            Faq faq = faqs.get(position);

            holder.question.setText(faq.question);
            holder.answer.setText(faq.answer);
        }

        @Override
        public int getItemCount()
        {
            return faqs.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder
        {
            public TextView question;
            public TextView answer;


            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);

                question = itemView.findViewById(R.id.faq_question);
                answer = itemView.findViewById(R.id.faq_answer);
            }

        }
    }
}