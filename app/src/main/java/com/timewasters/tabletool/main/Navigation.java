package com.timewasters.tabletool.main;

import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.MainActivity;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.main.market.MarketActivity;
import com.timewasters.tabletool.main.meeting.search.MeetingsSearchActivity;

public class Navigation
{
    private BottomNavigationView navigation;

    public Navigation(AppCompatActivity activity, int id)
    {
        navigation = activity.findViewById(R.id.bottomNav_view);
        navigation.setSelectedItemId(id);

        navigation.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                int id = item.getItemId();

                if (id == R.id.person)
                {
                    ActivityStarter.startActivity(activity, MainActivity.class);

                    return true;
                }
                else
                if (id == R.id.home)
                {
                    ActivityStarter.startActivity(activity, MarketActivity.class);

                    return true;
                }
                else
                if (id == R.id.settings)
                {
                    ActivityStarter.startActivity(activity, MeetingsSearchActivity.class);

                    return true;
                }

                return false;
            }
        });
    }
}
