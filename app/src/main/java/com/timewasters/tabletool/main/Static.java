package com.timewasters.tabletool.main;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.WelcomeActivity;
import com.timewasters.tabletool.error.OSError;
import com.timewasters.tabletool.service.dto.game.GameDTO;
import com.timewasters.tabletool.service.dto.user.IsUniqueNicknameDTO;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Response;

public class Static
{
    public static String city = null;
    public static String rating = null;
    public static String nick = null;

    public static List<GameDTO> games = null;

    public static List<String> categories = new ArrayList<>();
    public static final List<String> cities = Arrays.asList("Воронеж", "Даларан", "Стратхольм", "Мос Айсли");
    public static List<String> ratings = Arrays.asList("0", "4");
    public static String filterCity = null;

    public static <T extends AppCompatActivity> void signOut(T previousActivity)
    {
        FirebaseAuth.getInstance().signOut();

        city = null;
        rating = null;
        nick = null;

        games = null;

        categories = new ArrayList<>();
        filterCity = null;

        ActivityStarter.startActivity(previousActivity, WelcomeActivity.class);
    }

    public static void showPopup(View view, AppCompatActivity thisReference)
    {
        PopupMenu popupMenu = new PopupMenu(thisReference, view);
        popupMenu.inflate(R.menu.about_menu);

        popupMenu.setOnMenuItemClickListener(menuItem ->
        {
            switch (menuItem.getItemId())
            {
                case R.id.about:
                    ActivityStarter.startActivity(thisReference, AboutActivity.class);

                    return true;

                case R.id.faq:
                    ActivityStarter.startActivity(thisReference, FAQActivity.class);

                    return true;

                default:
                    return false;
            }
        });

        popupMenu.show();
    }

    public static Drawable LoadImageFromWebOperations(String url)
    {
        final Drawable[] drawable = new Drawable[1];

        Thread thread = new Thread (()->
        {
            try
            {
                try
                {
                    InputStream is = (InputStream) new URL(url).getContent();
                    drawable[0] = Drawable.createFromStream(is, "src name");
                }
                catch (Exception e)
                {
                    drawable[0] = null;
                }
            }
            catch (Exception ignored) {}
        });

        thread.start();

        try
        {
            thread.join();
        }
        catch (Exception ignored) {}

        return drawable[0];
    }
}
