package com.timewasters.tabletool.main.city;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.ItemClickListener;
import com.timewasters.tabletool.MainActivity;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.WelcomeActivity;
import com.timewasters.tabletool.main.Static;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PickCityActivity extends AppCompatActivity implements ItemClickListener
{
    private FirebaseAuth mAuthenticator;

    private RecyclerView cityList;
    private List<String> mCities;

    private CitiesAdapter adapter;

    private TextView currentItem;

    private ImageButton backButton;
    private ImageButton moreOptionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_city_page);

        mCities = Static.cities;
        currentItem = null;

        mAuthenticator = FirebaseAuth.getInstance();

        initializeLayoutVariables();

        moreOptionsButton.setOnClickListener(view ->
                Static.showPopup(view, this));

        cityList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CitiesAdapter(this, mCities);
        adapter.setClickListener(this);

        cityList.setAdapter(adapter);

        backButton.setOnClickListener(view ->
                {
                    UserProfileChangeRequest update = new UserProfileChangeRequest.Builder().setDisplayName(Static.city).build();

                    mAuthenticator.getCurrentUser().updateProfile(update).addOnCompleteListener(task ->
                    {
                        if (!task.isSuccessful())
                        {
                            mAuthenticator.signOut();

                            ActivityStarter.startActivity(this, WelcomeActivity.class);
                        }
                    });

                    ActivityStarter.startActivity(this, MainActivity.class);
                });
    }

    void initializeLayoutVariables()
    {
        cityList = findViewById(R.id.city_list);

        backButton = findViewById(R.id.back_button);
        moreOptionsButton = findViewById(R.id.more_opt_but);
    }

    @Override
    public void onItemClick(View view, int position)
    {
        if (currentItem != null)
        {
            currentItem.setTextColor(Color.parseColor("#FFFFFFFF"));

            currentItem = adapter.view(position);

            currentItem.setTextColor(Color.parseColor("#FF2879FA"));
        }
        else
        {
            currentItem = adapter.view(position);

            currentItem.setTextColor(Color.parseColor("#FF2879FA"));
        }

        Static.city = mCities.get(position);
    }

    class CitiesAdapter extends RecyclerView.Adapter<PickCityActivity.CitiesAdapter.ViewHolder>
    {
        private final List<String> mCities;
        private final LayoutInflater mLayoutInflater;
        private ItemClickListener mItemClickListener;

        private ArrayList<TextView> items;

        CitiesAdapter(Context context, List<String> cities)
        {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.mCities = cities;

            items = new ArrayList<>();
        }

        @NonNull
        @Override
        public PickCityActivity.CitiesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = mLayoutInflater.inflate(R.layout.change_city_item, parent, false);

            PickCityActivity.CitiesAdapter.ViewHolder holder = new PickCityActivity.CitiesAdapter.ViewHolder(view);

            items.add(holder.city);

            if (Static.city != null && !Static.city.isEmpty() && items.size() - 1 >= mCities.indexOf(Static.city))
            {
                currentItem = adapter.view(mCities.indexOf(Static.city));

                currentItem.setTextColor(Color.parseColor("#FF2879FA"));
            }

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull PickCityActivity.CitiesAdapter.ViewHolder holder, int position)
        {
            String city = mCities.get(position);

            holder.city.setText(city);
        }

        @Override
        public int getItemCount()
        {
            return mCities.size();
        }

        public TextView view(int index)
        {
            return items.get(index);
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {
            public TextView city;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);

                city = itemView.findViewById(R.id.city_name);

                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view)
            {
                if (mItemClickListener != null)
                    mItemClickListener.onItemClick(view, getAdapterPosition());
            }
        }

        void setClickListener(ItemClickListener itemClickListener)
        {
            this.mItemClickListener = itemClickListener;
        }
    }
}