package com.timewasters.tabletool.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;

import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.MainActivity;
import com.timewasters.tabletool.R;

public class AboutActivity extends AppCompatActivity
{
    ImageButton backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_page);

        backButton = findViewById(R.id.back_button);

        backButton.setOnClickListener(view ->
                ActivityStarter.startActivity(this, MainActivity.class));
    }
}