package com.timewasters.tabletool.main.settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.MainActivity;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.error.AuthenticationError;
import com.timewasters.tabletool.error.ServerError;
import com.timewasters.tabletool.service.UsersService;
import com.timewasters.tabletool.service.dto.user.UpdateUserDTO;
import com.timewasters.tabletool.service.dto.user.UserDTO;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeNicknameActivity extends AppCompatActivity
{
    private FirebaseAuth mAuthenticator;
    private UsersService mUsersService;

    private ImageButton previousScreenButton;

    private EditText newNicknameText;
    private EditText passwordConfirmationText;
    private TextView errorText;

    private Button changeNicknameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_nickname_page);

        ChangeNicknameActivity thisReference = this;
        mAuthenticator = FirebaseAuth.getInstance();

        initializeLayoutVariables();

        previousScreenButton.setOnClickListener((view ->
                ActivityStarter.startActivity(this, SettingsActivity.class)));

        changeNicknameButton.setOnClickListener((view ->
        {
            if (newNicknameText.getText().toString().isEmpty())
            {
                errorText.setText(AuthenticationError.EMPTY_NICKNAME.toString());

                return;
            }

            if (passwordConfirmationText.getText().toString().isEmpty())
            {
                errorText.setText(AuthenticationError.EMPTY_PASSWORD_CONFIRMATION.toString());

                return;
            }

            FirebaseUser user = mAuthenticator.getCurrentUser();

            AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(), passwordConfirmationText.getText().toString());

            user.reauthenticate(credential).addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                {
                    mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task1 ->
                    {
                        if (task1.isSuccessful())
                        {
                            String token = task1.getResult().getToken();

                            mUsersService = Client.getClient(token).create(UsersService.class);

                            Call<UserDTO> user1 = mUsersService.updateUser(new UpdateUserDTO(newNicknameText.getText().toString(), mAuthenticator.getCurrentUser().getUid()));

                            user1.enqueue(new Callback<UserDTO>()
                            {
                                @Override
                                public void onResponse(@NonNull Call<UserDTO> call, @NonNull Response<UserDTO> response)
                                {
                                    if (response.isSuccessful())
                                    {
                                        ActivityStarter.startActivity(thisReference, MainActivity.class);
                                    }
                                    else
                                        errorText.setText(AuthenticationError.NOT_UNIQUE_NICKNAME.toString());

                                    clearFields();
                                }

                                @Override
                                public void onFailure(@NonNull Call<UserDTO> call, @NonNull Throwable t)
                                {
                                    errorText.setText(ServerError.SERVER_UNREACHABLE.toString());

                                    clearFields();
                                }
                            });
                        }
                        else
                        {
                            errorText.setText(ServerError.SERVER_UNREACHABLE.toString());

                            clearFields();
                        }
                    });
                }
                else
                {
                    errorText.setText(AuthenticationError.WRONG_PASSWORD.toString());

                    clearFields();
                }
            });
        }));
    }

    private void initializeLayoutVariables()
    {
        previousScreenButton = findViewById(R.id.back_button);
        newNicknameText = findViewById(R.id.new_nickname_field);
        passwordConfirmationText = findViewById(R.id.pass_conf_field);
        errorText = findViewById(R.id.error_text_block2);
        changeNicknameButton = findViewById(R.id.change_nickname_button);
    }

    private void clearFields()
    {
        newNicknameText.getText().clear();
        passwordConfirmationText.getText().clear();
    }
}