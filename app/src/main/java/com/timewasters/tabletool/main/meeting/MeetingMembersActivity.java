package com.timewasters.tabletool.main.meeting;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.timewasters.tabletool.ActivityStarter;
import com.timewasters.tabletool.R;
import com.timewasters.tabletool.WelcomeActivity;
import com.timewasters.tabletool.client.Client;
import com.timewasters.tabletool.main.Static;
import com.timewasters.tabletool.main.meeting.search.SearchMeetingActivity;
import com.timewasters.tabletool.service.MeetingService;
import com.timewasters.tabletool.service.UsersService;
import com.timewasters.tabletool.service.dto.meeting.MeetingDTO;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeetingMembersActivity extends AppCompatActivity
{
    FirebaseAuth mAuthenticator;

    List<String> members;
    List<String> memberIds;
    List<String> ratings;

    String meetingId;
    Boolean isCreator;

    RecyclerView memberList;
    MembersAdapter adapter;

    ImageButton backButton;
    ImageButton moreOptionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_members_page);

        mAuthenticator = FirebaseAuth.getInstance();

        initializeLayoutVariables();

        Bundle info = getIntent().getExtras();

        members = (List<String>)info.getStringArrayList("members").clone();
        memberIds = (List<String>)info.getStringArrayList("membersIds").clone();
        ratings = (List<String>)info.getStringArrayList("membersRatings").clone();

        meetingId = info.getString("meetingId");

        int currentUserIndex = members.indexOf(Static.nick);
        if (members.contains(Static.nick))
        {
            members.remove(currentUserIndex);
            memberIds.remove(currentUserIndex);
            ratings.remove(currentUserIndex);
        }

        isCreator = info.getString("organizerId").equals(mAuthenticator.getCurrentUser().getUid());

        memberList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MembersAdapter(this, members, memberIds, ratings, meetingId, this, isCreator);

        memberList.setAdapter(adapter);

        if (!info.containsKey("search"))
        {
            backButton.setOnClickListener(view ->
                    ActivityStarter.startActivity(this, MeetingActivity.class, info));
        }
        else
        {
            backButton.setOnClickListener(view ->
                    ActivityStarter.startActivity(this, SearchMeetingActivity.class, info));
        }

        moreOptionsButton.setOnClickListener(view ->
                Static.showPopup(view, this));
    }

    private void initializeLayoutVariables()
    {
        memberList = findViewById(R.id.game_members_list);

        backButton = findViewById(R.id.back_button);
        moreOptionsButton = findViewById(R.id.more_opt_but);
    }

    public void reinitializePlayers()
    {
        MeetingMembersActivity thisReference = this;

        mAuthenticator.getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
        {
           if (task.isSuccessful())
           {
               String token = task.getResult().getToken();

               MeetingService meetingService = Client.getClient(token).create(MeetingService.class);

               Call<MeetingDTO> meetingById = meetingService.meetingById(meetingId);

               meetingById.enqueue(new Callback<MeetingDTO>()
               {
                   @Override
                   public void onResponse(@NonNull Call<MeetingDTO> call, @NonNull Response<MeetingDTO> response)
                   {
                       Bundle info = response.body().pack();

                       members = info.getStringArrayList("members");
                       memberIds = info.getStringArrayList("membersIds");
                       ratings = info.getStringArrayList("membersRatings");

                       int currentUserIndex = members.indexOf(Static.nick);
                       if (members.contains(Static.nick))
                       {
                           members.remove(currentUserIndex);
                           memberIds.remove(currentUserIndex);
                           ratings.remove(currentUserIndex);
                       }

                       memberList.setAdapter(new MembersAdapter(thisReference, members, memberIds,
                               ratings, meetingId, thisReference, isCreator));
                   }

                   @Override
                   public void onFailure(@NonNull Call<MeetingDTO> call, @NonNull Throwable t)
                   {
                       Static.signOut(thisReference);
                   }
               });
           }
           else
           {
               Static.signOut(thisReference);
           }
        });
    }

    static class MembersAdapter extends RecyclerView.Adapter<MeetingMembersActivity.MembersAdapter.ViewHolder>
    {
        List<String> members;
        List<String> memberIds;
        List<String> ratings;
        Boolean isCreator;

        String meetingId;

        private final LayoutInflater mLayoutInflater;

        private final MeetingMembersActivity thisReference;

        MembersAdapter(Context context, List<String> members, List<String> memberIds,
                       List<String> ratings, String meetingId, MeetingMembersActivity thisReference, Boolean isCreator)
        {
            this.mLayoutInflater = LayoutInflater.from(context);
            this.members = members;
            this.memberIds = memberIds;
            this.ratings = ratings;
            this.thisReference = thisReference;
            this.meetingId = meetingId;
            this.isCreator = isCreator;
        }

        @NonNull
        @Override
        public MeetingMembersActivity.MembersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            int layoutIndex = isCreator ? R.layout.game_members__creator_item : R.layout.game_members_item;

            View view = mLayoutInflater.inflate(layoutIndex, parent, false);

            return new ViewHolder(view, isCreator);
        }

        @Override
        public void onBindViewHolder(@NonNull MeetingMembersActivity.MembersAdapter.ViewHolder holder, int position)
        {
            String member = members.get(position);
            String rating = ratings.get(position);
            String id = memberIds.get(position);

            holder.member.setText(member);
            holder.rating.setText(rating);
            holder.userId = id;
            holder.meetingId = meetingId;
            holder.thisReference = thisReference;
        }

        @Override
        public int getItemCount()
        {
            return members.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder
        {
            public TextView member;
            public TextView rating;
            public String userId;
            public String meetingId;
            public ImageButton deletePlayer;

            public MeetingMembersActivity thisReference;

            public ViewHolder(@NonNull View itemView, Boolean isCreator)
            {
                super(itemView);

                member = itemView.findViewById(R.id.player);
                rating = itemView.findViewById(R.id.rating);

                if (isCreator)
                {
                    deletePlayer = itemView.findViewById(R.id.like_button);

                    deletePlayer.setOnClickListener(view ->
                            deletePlayer());
                }
            }

            private void deletePlayer()
            {
                FirebaseAuth.getInstance().getCurrentUser().getIdToken(true).addOnCompleteListener(task ->
                {
                   if (task.isSuccessful())
                   {
                       String token = task.getResult().getToken();

                       UsersService usersService = Client.getClient(token).create(UsersService.class);

                       Call<Void> deleteUserFromMeeting = usersService.deleteFromMeeting(meetingId, userId);

                       deleteUserFromMeeting.enqueue(new Callback<Void>()
                       {
                           @Override
                           public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response)
                           {
                                if (response.isSuccessful())
                                {
                                    Toast.makeText(thisReference, "Участник успешно удален", Toast.LENGTH_LONG).show();

                                    thisReference.reinitializePlayers();
                                }
                                else
                                {
                                    Toast.makeText(thisReference, "Не удалось удалить участника", Toast.LENGTH_LONG).show();
                                }
                           }

                           @Override
                           public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t)
                           {
                               Static.signOut(thisReference);
                           }
                       });
                   }
                   else
                   {
                       Static.signOut(thisReference);
                   }
                });
            }
        }
    }
}