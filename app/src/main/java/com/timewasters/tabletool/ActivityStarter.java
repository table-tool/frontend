package com.timewasters.tabletool;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.timewasters.tabletool.authentication.LoginActivity;
import com.timewasters.tabletool.authentication.PasswordResetActivity;
import com.timewasters.tabletool.authentication.PasswordResetConfirmationActivity;
import com.timewasters.tabletool.authentication.RegistrationActivity;

public class ActivityStarter
{
    public static <PrevActivityType extends AppCompatActivity> void startMainActivity(PrevActivityType previousActivityReference)
    {
        Intent intent = new Intent(previousActivityReference, MainActivity.class);

        previousActivityReference.startActivity(intent);
        previousActivityReference.overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }

    public static <PrevActivityType extends AppCompatActivity> void startLoginActivity(PrevActivityType previousActivityReference)
    {
        Intent intent = new Intent(previousActivityReference, LoginActivity.class);

        previousActivityReference.startActivity(intent);
        previousActivityReference.overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }

    public static <PrevActivityType extends AppCompatActivity> void startRegistrationActivity(PrevActivityType previousActivityReference)
    {
        Intent intent = new Intent(previousActivityReference, RegistrationActivity.class);

        previousActivityReference.startActivity(intent);
        previousActivityReference.overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }

    public static <PrevActivityType extends AppCompatActivity> void startWelcomeActivity(PrevActivityType previousActivityReference)
    {
        Intent intent = new Intent(previousActivityReference, WelcomeActivity.class);

        previousActivityReference.startActivity(intent);
        previousActivityReference.overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }

    public static <PrevActivityType extends AppCompatActivity> void startPasswordResetActivity(PrevActivityType previousActivityReference)
    {
        Intent intent = new Intent(previousActivityReference, PasswordResetActivity.class);

        previousActivityReference.startActivity(intent);
        previousActivityReference.overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }

    public static <PrevActivityType extends AppCompatActivity> void startPasswordResetConfirmationActivity(PrevActivityType previousActivityReference)
    {
        Intent intent = new Intent(previousActivityReference, PasswordResetConfirmationActivity.class);

        previousActivityReference.startActivity(intent);
        previousActivityReference.overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }

    public static <PrevActivityType extends AppCompatActivity, Activity> void startActivity(PrevActivityType previousActivityReference, Activity activity)
    {
        Intent intent = new Intent(previousActivityReference, (Class<?>)activity);

        previousActivityReference.startActivity(intent);
        previousActivityReference.overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }

    public static <PrevActivityType extends AppCompatActivity, Activity> void startActivity(PrevActivityType previousActivityReference, Activity activity, Bundle extras)
    {
        Intent intent = new Intent(previousActivityReference, (Class<?>)activity);

        intent.putExtras(extras);

        previousActivityReference.startActivity(intent);
        previousActivityReference.overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }
}
