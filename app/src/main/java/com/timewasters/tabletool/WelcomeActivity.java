package com.timewasters.tabletool;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

import com.timewasters.tabletool.main.market.MarketActivity;


public class WelcomeActivity extends AppCompatActivity
{
    private Button mGotoLoginScreenButton;
    private Button mGotoRegistrationScreenButton;
    private Button mGotoUnloggedMarketScreenButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_page);

        initializeLayoutVariables();

        WelcomeActivity thisReference = this;

        mGotoLoginScreenButton.setOnClickListener(view -> ActivityStarter.startLoginActivity(thisReference));

        mGotoRegistrationScreenButton.setOnClickListener(view -> ActivityStarter.startRegistrationActivity(thisReference));

        mGotoUnloggedMarketScreenButton.setOnClickListener(view ->
        {
            ActivityStarter.startActivity(this, MarketActivity.class);
        });
    }

    private void initializeLayoutVariables()
    {
        mGotoLoginScreenButton = findViewById(R.id.login_button);
        mGotoRegistrationScreenButton = findViewById(R.id.registration_button);
        mGotoUnloggedMarketScreenButton = findViewById(R.id.welcome_skip_button);
    }
}