package com.timewasters.tabletool.error;

import androidx.annotation.NonNull;

public enum AuthenticationError
{
    EMPTY_EMAIL("Поле почты не заполнено"),
    EMPTY_NICKNAME("Поле никнейма не заполнено"),
    NOT_UNIQUE_NICKNAME("Такой никнейм уже занят"),
    EMPTY_PASSWORD("Поле пароля не заполнено"),
    SHORT_PASSWORD("Пароль должен состоять не менее чем из 6 симоволов"),
    EMPTY_PASSWORD_CONFIRMATION("Поле подтверждения пароля не заполнено"),
    PASSWORDS_DO_NOT_MATCH("Пароли не совпадают"),
    WRONG_PASSWORD("Пароль указан неверно"),
    WRONG_EMAIL_OR_PASSWORD("Почта или пароль указаны неверно"),
    REGISTRATION_FAILED("Такой почты не существует или пароль содержит меньше 6 символов"),
    EMAIL_DO_NOT_EXIST("Указанная почта не существует либо не поддерживается"),
    EMAIL_UPDATE_FAILED("Указанная почта не существует либо уже используется");

    private final String text;

    AuthenticationError(final String text)
    {
        this.text = text;
    }

    @NonNull
    @Override
    public String toString()
    {
        return text;
    }
}
