package com.timewasters.tabletool.error;

import androidx.annotation.NonNull;

public enum MeetingError
{
    EMPTY_CITY("Выберите город на главном экране"),
    EMPTY_GAME("Игра не выбрана"),
    EMPTY_TIME("Время не выбрано"),
    EMPTY_ADDRESS("Адрес не указан"),
    EMPTY_DESCRIPTION("Описание не указано"),
    TIME("Путешествовать во времени можно только вперед"),
    ONE_MAN_ARMY("Одному скучно не будет?"),
    EMPTY_DATE("Дата не выбрана");

    private final String text;

    MeetingError(final String text)
    {
        this.text = text;
    }

    @NonNull
    @Override
    public String toString()
    {
        return text;
    }
}
