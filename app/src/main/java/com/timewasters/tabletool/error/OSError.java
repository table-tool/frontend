package com.timewasters.tabletool.error;

import androidx.annotation.NonNull;

public enum OSError
{
    THREAD_ERROR ("Внутренняя ошибка");

    private final String text;

    OSError(final String text)
    {
        this.text = text;
    }

    @NonNull
    @Override
    public String toString()
    {
        return text;
    }
}