package com.timewasters.tabletool.error;

import androidx.annotation.NonNull;

public enum ServerError
{
    SERVER_UNREACHABLE("Сервис не доступен"),
    SERVER_INTERNAL("Целостность данных вашего аккаунта была нарушена, теперь живите в этом проклятом мире, что сам разработчик и создал"),
    WRONG_INPUT("Введенные данные не валидны");

    private final String text;

    ServerError(final String text) {
        this.text = text;
    }

    @NonNull
    @Override
    public String toString() {
        return text;
    }
}
